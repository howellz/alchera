﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Handle Winning & losing, changing of screens, pausing, sending out events..
    /// </summary>
    public class WinLoseEffects : Singleton<WinLoseEffects>
    {
        //Texts in the menu whose values should be updated
        public TextMesh scoreMesh;
        public TextMesh goldMesh;
        public TextMesh highScoreMesh;
        public TextChangerShadow endlessMesh;

        //References to inactive objects to activate later
        public GameObject winScreen;
        public GameObject scoreScreen;
        public GameObject gameOver;
        public GameObject backGround;

        public GameObject floor;

        public float highScore;

        public Pauser pauser;

        //ending animation numbers
        protected float enjoyTimeWin = 3.3f;
        protected float enjoyTimeGameOver = 2.0f;
        protected float percentageAnimatedEarly = 0.5f;

        //Call these events when GameOver/Win happens
        //i here is the current level
        public IList<Action<int>> gameOverEvents = new List<Action<int>>();
        public IList<Action<int>> winEvents = new List<Action<int>>();

		protected void Start()
		{
            if(pauser==null)
            {
                pauser = Pauser.Instance;
            }

            winEvents.Add(delegate(int level){
                if(level==0)
                {
                    //record beating of tutorial
                    FileInput fileIO = FileInput.Instance;
                    UserPreferences progress = fileIO.ReadAndDeserialize<UserPreferences>(UserPreferences.playedBeforeFile).FirstOrDefault();
                    if(progress == null)
                    {
                        progress = new UserPreferences();
                    }
                    progress.beatTutorial = true;
                    fileIO.WriteAndSerialize<UserPreferences>(new List<UserPreferences>(){progress}, UserPreferences.playedBeforeFile);
                }
            });
		}
		
        //Blow up all the blocks in a pretty fashion!
        protected void ActOnRandomBlocks(Action<Collider2D, float> blockAction, float explosionTime)
        {
            Collider2D[] blocks = BlockParent.Instance.GetComponentsInChildren<Collider2D>();
            Collider2D[] randBlockOrder = RandomArrayTool.Randomize<Collider2D>(blocks);

            int numBlocks = randBlockOrder.Count() - 1;

            //stop here for some variety
            int actOnAllPoint = numBlocks - (int)(numBlocks * percentageAnimatedEarly);

            //work through array backwards in case we're destroying the blocks
            for (int i = numBlocks; i >= actOnAllPoint; i--)
            {
                blockAction(randBlockOrder[i],UnityEngine.Random.Range(0f, explosionTime));
            }

            //finish off the other blocks
            if (actOnAllPoint -1< 0)
                return;

            for(int i =actOnAllPoint-1;i>=0;i--)
            {
                blockAction(randBlockOrder[i],explosionTime);
            }
        }

        //Do stuff that needs to be done for both Win & GameOver
        protected void StopGame(float enjoyTime)
        {
            //do animations and shut the game play down
            Invoke("ActivateBackground", enjoyTime);
            pauser.Invoke("GameOver", enjoyTime);
            SpawnerObject.Instance.gameObject.SetActive(false);
            PlayerObject.Instance.gameObject.SetActive(false);
        }

        protected void ActivateBackground()
        {
            backGround.SetActive(true);
            scoreScreen.SetActive(true);
            UpdateScores();
        }

        protected bool gameComplete = false;

        public void Win()
        {
            if (gameComplete || !DifficultyToolbox.Instance.GetComponent<EndlessManager>().TryToWin())
            {
                return;
            }
            gameComplete = true;

            StopGame(enjoyTimeWin);

            float doneExploding = enjoyTimeWin * 3f / 4f;
            ActOnRandomBlocks(delegate(Collider2D block, float time)
            {
                GameObject.Destroy(block.gameObject, time);
            },doneExploding);

            Invoke("ShowWin", enjoyTimeWin);

            foreach (var action in winEvents)
            {
                action(LevelChanger.currentLevel);
            }
        }

        protected void ShowWin()
        {
            winScreen.SetActive(true);
        }

        public void GameOver()
        {
            if (gameComplete)
            {
                return;
            }
            gameComplete = true;

            StopGame(enjoyTimeGameOver);

            //floor.SetActive(false);
            float doneExploding = enjoyTimeGameOver * 3f / 5f;
            ActOnRandomBlocks(delegate(Collider2D block, float time)
            {
                block.renderer.sortingLayerID = 1;
                Collider2D.Destroy(block, time);
            },doneExploding);

            Invoke("ShowGameOver", enjoyTimeGameOver);

            foreach (var action in gameOverEvents)
            {
                action(LevelChanger.currentLevel);
            }
        }

        protected void ShowGameOver()
        {
            gameOver.SetActive(true);
        }

        /// <summary>
        /// Calculate/save final score totals by referencing BestScoreManager
        /// And update what we're actually displaying on gameover/win screen
        /// </summary>
        protected void UpdateScores()
        {
            ScoreManager scoreManager = ScoreManager.Instance;
            scoreManager.GetComponent<BestScoreManager>().SaveBestScore(LevelChanger.currentLevel);

            if (scoreMesh != null)
            {
                var textWShadow = scoreMesh.GetComponent<TextChangerShadow>();
                textWShadow.ChangeText(scoreManager.scoreVal.ToString());
            }
            if (goldMesh != null)
            {
                var textWShadow = goldMesh.GetComponent<TextChangerShadow>();
                textWShadow.ChangeText(scoreManager.goldVal.ToString());
            }
            if(highScoreMesh!=null)
            {
                var textWShadow = highScoreMesh.GetComponent<TextChangerShadow>();
                textWShadow.ChangeText(highScore.ToString());
                highScoreMesh.text = highScore.ToString();
            }
        }
    }
}
