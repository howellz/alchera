﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class GoalBannerSpawner : MonoBehaviour
    {
        public List<GameObject> goalBanners;
        public GameObject spawnPoint;

        protected void Start()
        {
            GameObject banner = goalBanners[LevelChanger.currentLevel];
            if(banner!=null)
            {
                GameObject.Instantiate(banner);
            }
        }
    }
}
