﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class HardModeUnlocker : MonoBehaviour
    {
        public string purchaseSKU = "hard_mode_unlock";
        public bool debug = false;
        
        public Action<bool> SetProductOwnership;

        public IPurchaser purchaser;

        protected void Start()
        {
            SetProductOwnership += WriteHardMode;
            if (Application.platform == RuntimePlatform.Android)
            {
                LogOut("Start - platform is android");
                purchaser = gameObject.AddComponent<AndroidPurchaser>();
                SetUpAndCheckPurchaser();
            }
            else
            {
                LogOut("Start - not Android");
                //everyone else gets game for free right now
                SetProductOwnership(true);
            }
        }

        protected void LogOut(string str)
        {
            if (debug)
            {
                Debug.Log(str);
            }
        }

        protected void SetUpAndCheckPurchaser()
        {
            purchaser.purchaseSKU = purchaseSKU;
            purchaser.OnInternetFailure = OnInternetFailure;
            purchaser.SetProductOwnership = SetProductOwnership;
            purchaser.CheckPurchase();
        }

        public void AttemptToPurchase()
        {
            purchaser.AttemptToPurchase();
        }

        /// ------------------ Backup player preferences version--------------------------------------
        /// 

        protected string fileName = "prefs";

        /// Check hard mode status by reading from a file
        protected void OnInternetFailure()
        {
            IEnumerable<HardModePurchased> hardPurchases = FileInput.Instance.ReadAndDeserialize<HardModePurchased>(fileName);
            if(hardPurchases!=null && hardPurchases.Count()!=0)
            {
                bool hardModeOn = hardPurchases.First().hmm;
                LogOut("internet failure set based on hmm "+hardModeOn);
                SetProductOwnership(hardModeOn);
            }
            else
            {
                LogOut("no file for internet failure to check");
                SetProductOwnership(false);
            }
        }

        protected void WriteHardMode(bool purchased)
        {
            HardModePurchased hardMode = new HardModePurchased
            {
                hmm = purchased
            };
            FileInput.Instance.WriteAndSerialize<HardModePurchased>(hardMode,fileName);
        }

        public class HardModePurchased
        {
            public bool hmm;
        }
    }
}
