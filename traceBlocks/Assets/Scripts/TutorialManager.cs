﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using ZachUtility;

namespace BlockGame
{
    /// <summary>
    /// Handle basically everything related to the tutorial
    /// This script is doing a lot of work and the tutorial is heavily scripted (ie has to go exactly one way)
    ///     Would be cool to expand some of this functionality into the rest of the game
    ///     
    /// Uses a lot of delegates that get called for the curMessage
    /// </summary>
    public class TutorialManager : MonoBehaviour
    {
        public List<GameObject> messages;

        public List<Transform> patterns;

        public delegate bool CheckCondition();

        public List<CheckCondition> advanceConditions;
        public List<Action> extraChecks;
        public List<Action> advanceActions;

        protected BlockParent blockParent;

        public int curMessage = 0;

        protected DifficultyIncreaser diffIncreaser;

        protected bool justStarted
        {
            get
            {
                return timeThisMsg <= 1.0f;
            }
        }

        protected void Start()
        {
            //initialize delegate lists
            //When should we advance to the next message?
            advanceConditions = new List<CheckCondition>();
            //Perform extra checks during update
            extraChecks = new List<Action>();
            //Extra actions to perform when going to next message
            advanceActions = new List<Action>();
            diffIncreaser = DifficultyToolbox.Instance.GetComponent<DifficultyIncreaser>();


            //Populate the events, they occur in order added

            //spawn 3 green blocks
            advanceConditions.Add(delegate()
            {
                //combined 3 green down
                return (blockParent.GetNumBlocksValue(BlockColor.Green) == 0);
            });
            extraChecks.Add(delegate() { });
            advanceActions.Add(delegate() { });

            //spawn "like colors".  a 4 & a 5 block set
            int numGoldAdvanceWave2 = 2;
            advanceConditions.Add(delegate()
            {
                //combined blue & green down
                return (blockParent.GetNumBlocksValue(2) <= 2 && blockParent.GetNumBlocksValue(4)>=numGoldAdvanceWave2);
            });
            extraChecks.Add(delegate() 
            {
                //restart if they don't combine 5
                /*int numBlueBlocks = blockParent.GetNumBlocksValue(2);
                if(numBlueBlocks<5 && numBlueBlocks>0)
                {
                    Retry();
                    numGoldAdvanceWave2 = 1;
                }*/
            });
            advanceActions.Add(delegate() 
            {
                ClearScreen();
            });

            //Spawn blue,green, gold tower
            advanceConditions.Add(delegate()
            {
                //combined all the blue
                return (blockParent.GetNumBlocksValue(2) == 0);
            });
            extraChecks.Add(delegate() { });
            advanceActions.Add(delegate(){});

            //Combine green from tower
            advanceConditions.Add(delegate()
            {
                //Made a third gold
                return (blockParent.GetNumBlocksValue(3) < 3);
            });
            extraChecks.Add(delegate() { });
            advanceActions.Add(delegate() { });

            //Combine the gold
            advanceConditions.Add(delegate()
            {
                //Everything gone
                return (blockParent.GetNumBlocksValue(4) == 0);
            });
            extraChecks.Add(delegate() { });
            advanceActions.Add(delegate()
            {
                //Start blocks falling with difficulty increaser
                //  Let the next action set know when to move on
                diffIncreaser.MoveToNextDifficulty();
                diffIncreaser.AddBeginDifficultyAction(2,
                    delegate()
                    {
                        Retry();
                        AdvanceMessage();
                    });
            });
            
            advanceConditions.Add(delegate()
            {
                //diff increaser will let use know when to move on
                return false;
            });
            extraChecks.Add(delegate() { });
            advanceActions.Add(delegate()
            {
                //set diff increaser gold value manually cause we don't know how many golds they've got so far
                diffIncreaser.numGoldWin = ScoreManager.Instance.goldVal + 12;
            });

            //Back to paused tutorial
            //Drop rainbows, make them combine them
            advanceConditions.Add(delegate()
            {
                //advance once rainbows cleared
                return (blockParent.GetNumBlocksValue(BlockFactory.rainbowNum)==0);
            });
            extraChecks.Add(delegate() { });
            advanceActions.Add(delegate()
            {
                //Just taught them about rainbows, continue
                diffIncreaser.MoveToNextDifficulty();
            });

            //No more tutorial stuff, just spawn randomly until they get 10 gold
            advanceConditions.Add(delegate()
            {
                //letting DifficultyIncreaser take over
                return false;
            });
            extraChecks.Add(delegate() { });
            advanceActions.Add(delegate(){});

            //Don't display endless message
            WinLoseEffects winLose = WinLoseEffects.Instance;
            winLose.winEvents.Add(delegate(int l)
            {
                winLose.endlessMesh.gameObject.SetActive(false);
            });

            blockParent = BlockParent.Instance;
            ActivateMessage(curMessage);
        }

        protected void Retry()
        {
            ClearScreen();
            //respawn the blocks
            ActivateMessage(curMessage);
        }

        protected void ClearScreen()
        {
            //Destory all the current blocks
            for (int i = blockParent.totalBlocks - 1; i >= 0; i -= 1)
            {
                GameObject.Destroy(blockParent.transform.GetChild(i).gameObject);
            }
        }

        protected float timeThisMsg = 0f;

        protected void Update()
        {
            //spawnTimer.Update();
            if(!justStarted)
            {
                if (curMessage < advanceConditions.Count && advanceConditions[curMessage]())
                {
                    AdvanceMessage();
                }
                if (curMessage < extraChecks.Count)
                {
                    extraChecks[curMessage]();
                }
            }
            timeThisMsg += Time.deltaTime;
        }

        protected void AdvanceMessage()
        {
            if(advanceActions.Count>curMessage)
            {
                advanceActions[curMessage]();
            }
            curMessage += 1;
            timeThisMsg = 0f;
            ActivateMessage(curMessage);
        }

        protected void ActivateMessage(int msgNum)
        {
            if(msgNum>=messages.Count)
            {
                return;
            }
            //Deactivate all messages, then activate selected message
            foreach (var message in messages)
            {
                message.SetActive(false);
            }
            messages[msgNum].SetActive(true);

            if(msgNum>=patterns.Count)
            {
                return;
            }

            //Spawn corresponding pattern
            Transform pattern = GameObject.Instantiate(patterns[msgNum], transform.position, Quaternion.identity) as Transform;
            pattern.parent = blockParent.transform;
        }
    }
}
