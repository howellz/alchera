﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class HardStartButton : LockedButton
    {
        public StartMenu startMenu;

        protected override void LockChangeAction()
        {
            if (locked)
            {
                startMenu.SetHardAction(delegate()
                {
                    LevelChanger.Instance.ChangeLevel(2);
                });
            }
            else
            {
                startMenu.SetHardAction(Purchase);
            }
        }

    }
}
