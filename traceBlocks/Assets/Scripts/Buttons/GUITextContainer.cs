using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent (typeof (GUIText))]
public class GUITextContainer : GUIContainer {
	
	//a list of textures we can switch between
	public List<string> texts;
	public GUIText guiText = null;
	
	override protected void Start (){
		//getcomponents
		guiText = GetComponent<GUIText>();
		
		//auto initialize to work intuitively with only one text string
		if(texts.Count == 0 && guiText.text!=null)
			texts.Add(guiText.text);
		
		guiText.text = texts[curContainer];
		base.Start();
	}
	
	//make the texture invisible
	override public bool visible
	{
		get
		{
			return (guiText.text!=null);
		}
		set
		{
			//only change values if they need changing
			if(value!=visible)
			{
				if(value)
					guiText.text = texts[curContainer];
				else guiText.text = null;
			}
		}
	}
	
    protected override void UpdateCurrentContainer()
    {
        guiText.text = texts[curContainer];
    }

}