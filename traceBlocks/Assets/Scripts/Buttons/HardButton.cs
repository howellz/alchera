﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class HardButton : MonoBehaviour
    {
        public StartMenu startMenu;
        protected HardModeUnlocker hardPurchaser;

        public bool hardmodeEnabled = false;

        public GameObject enabledHardButton;
        public GameObject disabledHardButton;

        protected void Awake()
        {
            hardPurchaser = gameObject.GetComponent<HardModeUnlocker>();
            hardPurchaser.SetProductOwnership = SetHardMode;
        }

        protected void Start()
        {
            SetHardMode(hardmodeEnabled);
        }

        public void SetHardMode(bool hardEnabled)
        {
            hardmodeEnabled = hardEnabled;
            enabledHardButton.SetActive(hardEnabled);
            disabledHardButton.SetActive(!hardEnabled);
            if(hardEnabled)
            {
                startMenu.SetHardAction(delegate()
                {
                    LevelChanger.Instance.ChangeLevel(2);
                });
            }
            else
            {
                startMenu.SetHardAction(PurchaseHardMode);
            }
        }

        public void PurchaseHardMode()
        {
            hardPurchaser.AttemptToPurchase();
        }
    }
}
