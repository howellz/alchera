using UnityEngine;
using System.Collections;

/// <summary>
/// Calls a delegate when pressed
///     delegate usually assigned by Menu script
///     makes IContainer visible or invisible 
///     IContainer is kinda silly, (attempting to do animation for clicked/unclicked I think).  Should probably do something different for a "Button"
/// </summary>
[RequireComponent (typeof (IContainer))]
public class GUIButton : MonoBehaviour
{
	//either define a delegate for the button using code
	public delegate void Method();
	public Method method;
	
	//or invoke a function with invokeName on the inspector defined scriptDestination gameObject
	public GameObject scriptDestination;
	public string invokeName;
	
	public IContainer icon;

    public AudioSource btnPressedSound;
	
	void Start()
	{
		icon = gameObject.GetComponent<IContainer>();
	}
	
	protected void OnMouseDown()
	{
        if (btnPressedSound != null)
        {
            btnPressedSound.Play();
        }
		callMethod();
	}
	
	public void callMethod()
	{
		if(method==null)
		{
			if(scriptDestination!=null && invokeName!="")
				scriptDestination.SendMessage(invokeName,SendMessageOptions.DontRequireReceiver);
			else
				throw new System.ArgumentException("Button's method is not defined");
		}
		else
			method();
	}
	
	//make the texture invisible
	public bool visible
	{
		get 
		{
			return icon.visible;
		}
		set
		{
			icon.visible = value;
		}
	}
	
	/*public void setTexture(Texture2D tex)
	{
		icon.setTexture(tex);
	}*/
	
	public void switchToNext()
	{
		icon.switchToNext();
	}
}

