﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class SpriteContainer : IContainer
    {
        public List<Sprite> containers;
        protected SpriteRenderer attachedSprite;

        protected void Awake()
        {
            attachedSprite = gameObject.GetComponent<SpriteRenderer>();
        }

        public override bool visible
        {
            get
            {
                return renderer.enabled;
            }
            set
            {
                renderer.enabled = value;
            }
        }

        protected override void UpdateCurrentContainer()
        {
            attachedSprite.sprite = containers[curContainer];
        }

    }
}
