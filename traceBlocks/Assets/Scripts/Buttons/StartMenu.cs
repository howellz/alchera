using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BlockGame;

[RequireComponent (typeof (Menu))]
public class StartMenu : MonoBehaviour {

	private Menu menuScript;

    public void SetHardAction(GUIButton.Method method)
    {
        menuScript.SetFunction(1, method);
    }

    public void SetEndlessAction(GUIButton.Method method)
    {
        menuScript.SetFunction(5, method);
    }
	
	void Awake () {
		menuScript = gameObject.GetComponent<Menu>();
        //easy
		menuScript.SetFunction(0,
			delegate()
			{
                ChangeToLevel(1);
			});
        //hard
        menuScript.SetFunction(1,
            delegate()
            {
                ChangeToLevel(2);
            });
        //quit
        menuScript.SetFunction(2,
            delegate()
            {
                Debug.Log("quitting");
                Application.Quit();
            });
        //tutorial
        menuScript.SetFunction(3, delegate()
            {
                ChangeToLevel(0);
            });
        //disabled tutorial
        menuScript.SetFunction(4, delegate()
            {
                ChangeToLevel(0);
            });
        //endless
        menuScript.SetFunction(5, delegate()
        {
            ChangeToLevel(3);
        });
	}

    protected void Start()
    {
        Screen.showCursor = true;

		Screen.showCursor = true;
	}

    protected void ChangeToLevel(int level)
    {
        LevelChanger.Instance.ChangeLevel(level);
    }
	
	// Update is called once per frame
	protected void Update () {
		if ( Input.GetKeyDown("enter") )
		{
			menuScript.callMethod(0);
		}
        if(Input.GetKeyDown("t"))
        {
            //Go to tutorial
            menuScript.callMethod(3);
            //LevelChanger.Instance.ChangeLevel(0);
        }
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            //Android back button
            Application.Quit(); 
        }
	}
}

/// <summary>
/// A random class that may eventually store saved info for user between play throughs
/// </summary>
public class UserPreferences
{
    public static string playedBeforeFile = "userPlayed";
    public bool beatTutorial = false;
}