﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class LockedButton : MonoBehaviour
    {
        protected HardModeUnlocker hardPurchaser;

        public bool locked = false;

        public GameObject enabledButton;
        public GameObject disabledButton;

        protected void Awake()
        {
            hardPurchaser = gameObject.GetComponent<HardModeUnlocker>();
            hardPurchaser.SetProductOwnership = SetLocked;
        }

        public void SetLocked(bool enabled)
        {
            locked = enabled;
            enabledButton.SetActive(enabled);
            disabledButton.SetActive(!enabled);
            LockChangeAction();
        }

        protected virtual void LockChangeAction()
        {
        }

        public void Purchase()
        {
            hardPurchaser.AttemptToPurchase();
        }
    }
}
