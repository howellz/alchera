﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class EndlessStartButton : LockedButton
    {
        public StartMenu startMenu;

        protected override void LockChangeAction()
        {
            if (locked)
            {
                startMenu.SetEndlessAction(delegate()
                {
                    LevelChanger.Instance.ChangeLevel((int)LevelChanger.Level.Endless);
                });
            }
            else
            {
                startMenu.SetEndlessAction(Purchase);
            }
        }

    }
}
