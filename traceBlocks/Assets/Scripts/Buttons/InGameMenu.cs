using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BlockGame;

[RequireComponent (typeof (Menu))]
public class InGameMenu : MonoBehaviour {

	private Menu menuScript;
    public Pauser pauser;
	
	void Awake () {
		menuScript = gameObject.GetComponent<Menu>();
		menuScript.SetFunction(0,
			delegate()
			{
                //Resume playing
                pauser.UnPause();
                AnalyticsSingleton.Instance.LogCurrentLevel();
				//Application.LoadLevel(Application.loadedLevel+1);
			});
        menuScript.SetFunction(1,
            delegate()
            {
                //Restart
                pauser.UnPause();
                LevelChanger.Instance.RestartLevel();
                //Application.LoadLevel(Application.loadedLevel);
            });
        menuScript.SetFunction(2,
            delegate()
            {
                //Go to main menu
                pauser.UnPause();
                LevelChanger.Instance.GoToMenu();
                //Application.LoadLevel(0);
            });
	}
}