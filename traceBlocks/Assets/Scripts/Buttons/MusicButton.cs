﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class MusicButton : MonoBehaviour
    {
        protected SpriteContainer container;
        protected bool musicOn = true;
        public string fileName = "musicPreferences";

        protected void Awake()
        {
            container = gameObject.GetComponent<SpriteContainer>();
        }

        protected void Start()
        {
            FileInput fIO = FileInput.Instance;
            IEnumerable<bool> musicList = fIO.ReadAndDeserialize<bool>(fileName);
            if(musicList!=null && musicList.Count()>0)
            {
                bool music = musicList.First();
                if(!music)
                {
                    SwitchMusic();
                }
            }
            else
            {
                WriteMusic();
            }
        }

        protected void WriteMusic()
        {
            FileInput.Instance.WriteAndSerialize<bool>(musicOn, fileName);
        }

        protected void SwitchMusic()
        {
            container.switchToNext();
            musicOn = !musicOn;
            if (musicOn)
            {
                AudioListener.volume = 1f;
            }
            else
            {
                AudioListener.volume = 0f;
            }
        }

        protected void OnMouseDown()
        {
            SwitchMusic();
            WriteMusic();
        }
    }
}
