using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public abstract class IContainer : MonoBehaviour {

    protected int curContainer = 0;
    protected int numContainers
    {
        get
        {
            return 2;
        }
    }

	//make the texture invisible
	public abstract bool visible
	{
		get;
		set;
	}
	
	public virtual void switchToNext()
    {
        curContainer += 1;
        if(curContainer>=numContainers)
        {
            curContainer = 0;
        }
        if(visible)
        {
            UpdateCurrentContainer();
        }
    }

    protected abstract void UpdateCurrentContainer();
}