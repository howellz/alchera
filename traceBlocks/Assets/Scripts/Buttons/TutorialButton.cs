﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class TutorialButton : MonoBehaviour
    {
        public bool tutorialRequired = false;

        public GameObject bigTutorialButton;
        public GameObject normalButtons;

        public static string playedBeforeFile = "userPlayed";
        public bool playedBefore;

        protected void Start()
        {
            FileInput fileIO = FileInput.Instance;
            UserPreferences prefs = fileIO.ReadAndDeserialize<UserPreferences>(UserPreferences.playedBeforeFile).FirstOrDefault();
            tutorialRequired = (prefs == null || prefs.beatTutorial == false);
            SetTutorialMode(tutorialRequired);
        }

        public void SetTutorialMode(bool tutorialOn)
        {
            tutorialRequired = tutorialOn;
            bigTutorialButton.SetActive(tutorialOn);
            normalButtons.SetActive(!tutorialOn);
        }

    }
}
