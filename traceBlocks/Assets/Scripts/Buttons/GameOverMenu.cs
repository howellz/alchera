using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BlockGame;

[RequireComponent (typeof (Menu))]
public class GameOverMenu : MonoBehaviour {

	private Menu menuScript;
    public Pauser pauser;
	
	void Awake () {
		menuScript = gameObject.GetComponent<Menu>();
        menuScript.SetFunction(0,
            delegate()
            {
                //Restart
                pauser.UnPause();
                LevelChanger.Instance.RestartLevel();
                //Application.LoadLevel(Application.loadedLevel);
            });
        menuScript.SetFunction(1,
            delegate()
            {
                //Go to main menu
                pauser.UnPause();
                LevelChanger.Instance.GoToMenu();
                //Application.LoadLevel(0);
            });
	}
	
	protected void Start(){
		pauser = Pauser.Instance;
	}
}