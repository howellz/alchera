using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BlockGame;

[RequireComponent (typeof (Menu))]
public class WinMenu : MonoBehaviour {

	private Menu menuScript;
    public Pauser pauser;
	
	protected void Awake () {
		menuScript = gameObject.GetComponent<Menu>();
        menuScript.SetFunction(0,
            delegate()
            {
                //Restart with next endless mode
                EndlessManager.StartEndless();
            });
        menuScript.SetFunction(1,
            delegate()
            {
                //Go to main menu
                pauser.UnPause();
                LevelChanger.Instance.GoToMenu();
                //Application.LoadLevel(0);
            });
	}
	
	protected void Start(){
        if(pauser==null)
        {
            pauser = Pauser.Instance;
        }
	}
}