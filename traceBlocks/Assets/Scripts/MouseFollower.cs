﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class MouseFollower : MonoBehaviour
    {
        protected void Update()
        {
            transform.position = InputMouse.worldPosition;
        }
    }
}
