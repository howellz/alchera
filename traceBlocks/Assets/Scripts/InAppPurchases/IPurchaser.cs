﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public abstract class IPurchaser : MonoBehaviour
    {
        [HideInInspector]
        public string purchaseSKU;

        //What action to take when a product's ownership status is set
        public Action<bool> SetProductOwnership;

        //Do this when not connected to the internet
        public Action OnInternetFailure;

        public abstract void CheckPurchase();

        public abstract void AttemptToPurchase();
    }
}
