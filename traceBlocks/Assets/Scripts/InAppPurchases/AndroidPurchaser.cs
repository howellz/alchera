﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class AndroidPurchaser : IPurchaser
    {
        protected AndroidInAppPurchaseManager appPurchaser;

        public bool debug = false;

        protected void LogStuff(string str)
        {
            if(debug)
            {
                Debug.Log(str);
            }
        }

        //Have we purchased the product?
        public override void CheckPurchase()
        {
            LogStuff("CheckPurchase");
            //While internet is loading, use local file action
            OnInternetFailure();

            //Only do this stuff for Android (really needs to go into a separate script though)
            appPurchaser = AndroidInAppPurchaseManager.instance;

            AndroidInAppPurchaseManager.ActionProductPurchased += OnProductPurchased;
            AndroidInAppPurchaseManager.ActionBillingSetupFinished += OnBillingConnected;
            if (!appPurchaser.IsConnectd)
            {
                LogStuff("app not connected");
                //we haven't loaded the store yet so do that
                appPurchaser.loadStore();
            }
            else if (!appPurchaser.IsInventoryLoaded)
            {
                LogStuff("inventory not loaded");
                //loaded the store, but not the inventory
                OnBillingConnectedSuccess();
            }
            else
            {
                LogStuff("connected to both");
                //connected to both store and inventory already
                OnRetrieveProductsSuccess();
            }
        }

        /// ------------------ Inapp billing purchasing --------------------------------------
        /// 

        public override void AttemptToPurchase()
        {
            appPurchaser.purchase(purchaseSKU);
        }

        /// Called after product purchase call made
        /// ->SetProductOwnership on success
        /// ->CheckPurchase on failure
        protected void OnProductPurchased(BillingResult result)
        {
            LogStuff("on product purchased result: " + result.message);
            SetProductOwnership(result.isSuccess);
            if (result.isFailure)
            {
                //maybe it worked but didn't go through?
                Invoke("CheckPuchase", 0.1f);
            }
        }

        /// <summary>
        /// Load Store Finished and we're connected to google play
        /// ->OnBillingConnectedSuccess
        /// ->OnInternetFailure on failure
        /// </summary>
        /// <param name="result"></param>
        protected void OnBillingConnected(BillingResult result)
        {
            LogStuff("OnBillingConnected");
            if (result.isSuccess)
            {
                //successful connection
                OnBillingConnectedSuccess();
            }
            else
            {
                OnInternetFailure();
            }
        }

        /// <summary>
        /// We are connected for sure
        /// Next retrieve details for the products
        /// ->OnRetrieveProducts
        /// </summary>
        protected void OnBillingConnectedSuccess()
        {
            LogStuff("OnBillingConnectedSuccess");
            appPurchaser.retrieveProducDetails();
            AndroidInAppPurchaseManager.ActionRetrieveProducsFinished += OnRetrieveProducts;
        }

        /// <summary>
        /// We have a list of products the user owns
        /// ->OnRetrieveProductsSuccess
        /// ->InternetFailure on failure
        /// </summary>
        /// <param name="result"></param>
        protected void OnRetrieveProducts(BillingResult result)
        {
            LogStuff("OnRetrieveProducts");
            if (result.isSuccess)
            {
                //We have retrieved the inventory
                OnRetrieveProductsSuccess();
            }
            else
            {
                OnInternetFailure();
            }
        }

        /// <summary>
        /// app purchaser/inventory has a list of products
        /// See if the Hard mode product has been purchased
        /// ->SetProductOwnership delegate (set elsewhere)
        /// </summary>
        protected void OnRetrieveProductsSuccess()
        {
            if (SetProductOwnership != null)
            {
                LogStuff("Hard OnRetrieveProduct success called");
                SetProductOwnership(appPurchaser.inventory.IsProductPurchased(purchaseSKU));
            }
            else
            {
                LogStuff("Please set SetProductOwnership in HardModeUnlocker");
            }
        }
    }
}
