﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Change difficulty based on which level was initially loaded - ie, did they click hard or easy?
    /// </summary>
    public class DifficultySelector : MonoBehaviour
    {
        public int currentLevel = 0;

        public void SetLevel(int level)
        {
            currentLevel = level;
        }

                /// <summary>
        /// Unity auto calls this when a scene is loaded
        /// I'm semi-manually handling scene/difficulty changes, so currentLevel matters more
        /// </summary>
        /// <param name="level">
        /// we don't really care about this value, this is what scene in Unity we are</param>
        protected virtual void OnLevelWasLoaded(int level)
        {
        }
    }
}
