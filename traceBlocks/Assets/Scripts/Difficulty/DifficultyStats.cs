﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Change the below values in their corresponding objects
    /// With the goal of increasing difficulty while staying in the same scene
    /// 
    /// The class knows where all the values go/has references to a lot of objects
    /// And performs some logic based on specific read-in values
    /// </summary>
    public class DifficultyStats
    {
        public float? minSpawnTime;
        public float? maxSpawnTime;
        public float? speedUpAmt;
        public float? hardFactor;
        public int? numBlocksInWave;
        public int? minBlockColor;
        public int? maxBlockColor;
        public int? numWavesSpeedUp;
        public float? speedUpTime;
        public float? difficultyTime;
        public int? numGoldWin;
        public int? numSpeedUpsDiff;
        public float? rainbowChance;
        public float? stoneChance;
        public List<float> blockChances;
        public int? minBlocksOnScreen;
        public int? numBlocksWin;
        public string beginMessage;
        //Should add chance of each type of block

        [JsonIgnore]
        TimedWavesManager manager;
        [JsonIgnore]
        SpeedUpManager speedUp;
        [JsonIgnore]
        BlockFactory factory;
        [JsonIgnore]
        SpeedUpTrigger speedTrigger;
        [JsonIgnore]
        DifficultyIncreaser difficultyIncreaser;
        [JsonIgnore]
        ContextSpawnTrigger contextSpawner;
        [JsonIgnore]
        MessageFactory messager;

        public DifficultyStats()
        {
            manager = SpawnerObject.Instance.GetOrAddComponent<TimedWavesManager>();
            speedUp = SpawnerObject.Instance.GetOrAddComponent<SpeedUpManager>();
            factory = Toolbox.Instance.GetOrAddComponent<BlockFactory>();
            speedTrigger = SpawnerObject.Instance.GetOrAddComponent<SpeedUpTrigger>();
            difficultyIncreaser = DifficultyToolbox.Instance.GetOrAddComponent<DifficultyIncreaser>();
            contextSpawner = BlockParent.Instance.GetOrAddComponent<ContextSpawnTrigger>();
            messager = Toolbox.Instance.GetOrAddComponent<MessageFactory>();
        }


        //Record values from actual game into difficulty stats object
        //  This is mainly for the intial creation of a difficulty stats object if none exist
        //  Rather than reproducing the game values that were set
        public static DifficultyStats GetGameValues()
        {
            DifficultyStats stats = new DifficultyStats();

            stats.minSpawnTime        =      stats.speedUp.minSpawnTime;
            stats.maxSpawnTime        =      stats.speedUp.startSpawnTime;
            stats.speedUpAmt          =      stats.speedUp.speedUpAmt;
            stats.numBlocksInWave     =      stats.manager.numBlocksInWave;
            stats.maxBlockColor       =      stats.factory.maxBlockSpawn;
            stats.minBlockColor       =      stats.factory.minBlockSpawn;
            stats.rainbowChance       =      stats.factory.rainbowChance;
            stats.stoneChance         =      stats.factory.stoneChance;
            stats.numGoldWin          =      stats.difficultyIncreaser.numGoldWin;
            stats.numSpeedUpsDiff     =      stats.speedTrigger.numSpeedUpsDiff;
            stats.minBlocksOnScreen   =      stats.contextSpawner.minBlocksOnScreen;
            stats.numBlocksWin        =      stats.difficultyIncreaser.numBlocksWin;
            stats.blockChances        =      stats.factory.blockChances;

            return stats;
        }

        //Assign values from DifficultyStats object to the actual game objects
        //If the value was not assigned in the DifficultyStats file, keep using the old value
        public void SetGameValues()
        {
            speedUp.SetTimes(minSpawnTime, maxSpawnTime, speedUpAmt);
            manager.numBlocksInWave         =       numBlocksInWave ?? manager.numBlocksInWave;
            factory.maxBlockSpawn           =       maxBlockColor ?? factory.maxBlockSpawn;
            factory.minBlockSpawn           =       minBlockColor ?? factory.minBlockSpawn;
            speedTrigger.SetNumWavesSpeedUp(numWavesSpeedUp);
            speedTrigger.SetSpeedUpTime(speedUpTime);
            speedTrigger.SetNumSpeedsUpDiff(numSpeedUpsDiff);
            speedTrigger.SetDifficultyTime(difficultyTime);
            factory.stoneChance             =       stoneChance ?? factory.stoneChance;
            factory.rainbowChance           =       rainbowChance ?? factory.rainbowChance;
            factory.blockChances            =       blockChances  ?? factory.blockChances;
            contextSpawner.minBlocksOnScreen=       minBlocksOnScreen ?? contextSpawner.minBlocksOnScreen;

            //Win conditions are nullable in destination objects as well
            //Allow -1 as a flag to set win conditions to actually be null
            if(numGoldWin==-1)
            {
                difficultyIncreaser.numGoldWin = null;
            }
            else
            {
                difficultyIncreaser.numGoldWin = numGoldWin ?? difficultyIncreaser.numGoldWin;
            }
            if(numBlocksWin==-1)
            {
                difficultyIncreaser.numBlocksWin = null;
            }
            else
            {
                difficultyIncreaser.numBlocksWin = numBlocksWin ?? difficultyIncreaser.numBlocksWin;
            }
            if(beginMessage!=null)
            {
                //Spawn a message with this text
                messager.WriteMessage(beginMessage);
            }
            //Debug.Log(ToString());
        }

        public string ToString()
        {
            string str = "";
            str += "hardFactor" + hardFactor;
            str += "minSpawnTime" + minSpawnTime;
            str += "maxSpawnTime" + maxSpawnTime;
            str += "numWavesSpeedUp"+numWavesSpeedUp;
            str += "numGoldWin" + numGoldWin;
            str += "numSpeedUpsDiff"+numSpeedUpsDiff;
            str += "stoneChance" + stoneChance;
            str += "rainbowChance" + rainbowChance;
            str += "minBlocksOnScreen" + minBlocksOnScreen;
            return str;
        }
    }
}
