﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Just calls DifficultyIncreaser to set initial level difficulty
    /// </summary>
    public class DifficultyTextSelector : DifficultySelector
    {
        protected DifficultyIncreaser difficultyIncreaser;

        //spawn this object in the new scene if tutorial
        public GameObject tutorialManagerPrefab;

        protected void Start()
        {
            difficultyIncreaser = gameObject.GetComponent<DifficultyIncreaser>();
        }

        /// <summary>
        /// Unity auto calls this when a scene is loaded
        /// I'm semi-manually handling scene/difficulty changes, so currentLevel matters more
        /// </summary>
        /// <param name="level">
        /// we don't really care about this value, this is what scene in Unity we are</param>
        protected override void OnLevelWasLoaded(int level) 
        {
            //Don't load difficulty in menu
            if(level!=0)
            {
                //Set the DifficultyIncreaser difficultyLevels list for it to read from
                if(currentLevel==0)
                {
                    difficultyIncreaser.SetDifficultyLevels(tutorialDifficulties);
                    GameObject.Instantiate(tutorialManagerPrefab);
                }
                else if(currentLevel==1)
                {
                    difficultyIncreaser.SetDifficultyLevels(easyDifficulties);
                }
                else if(currentLevel==2)
                {
                    difficultyIncreaser.SetDifficultyLevels(hardDifficulties); 
                }
                else
                {
                    DifficultyToolbox.Instance.GetComponent<EndlessManager>().TurnEndlessOn();
                    difficultyIncreaser.SetDifficultyLevels(endlessDifficulties);
                }
            }
        }

        //Separate lists are written out for easy use in Unity inspector
        //  Would use a List<List<TextAsset>> but Unity inspector can't read it
        //  And 
        //A list of difficulties to switch between once a difficulty is beaten
        public List<TextAsset> tutorialDifficulties;

        public List<TextAsset> easyDifficulties;

        public List<TextAsset> hardDifficulties;

        public List<TextAsset> endlessDifficulties;
    }
}
