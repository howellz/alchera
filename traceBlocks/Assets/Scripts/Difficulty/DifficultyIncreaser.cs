﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Detect DifficultyStats triggers to go to next difficulty and load in new DifficultyStats values
    /// </summary>
    public class DifficultyIncreaser : MonoBehaviour
    {
        //When user collects this much gold, go to next difficulty
        public int? numGoldWin
        {
            get
            {
                return _numGoldWin;
            }
            set
            {
                //change the goal value in the GUI
                if (value != null)
                {
                    goldScore.appendedText = "/" + value.ToString();
                }
                _numGoldWin = value;
            }
        }
        private int? _numGoldWin;
        //When fewer blocks than this on screen, go to next difficulty
        public int? numBlocksWin = null;

        //Gold stat from goldScore is total gold in all levels, offset with goldBeforeThisLevel
        protected int goldBeforeThisLevel = 0;
        protected Score goldScore
        {
            get
            {
                if (_goldScore == null)
                {
                    _goldScore = ScoreManager.Instance.gold;
                }
                return _goldScore;
            }
        }
        private Score _goldScore = null;
        protected int goldThisLevel 
        {
            get
            {
                return ((int)goldScore.score) - goldBeforeThisLevel;
            }
        }

        //number of blocks on screen right now
        protected int numBlocks
        {
            get
            {
                return blockParent.childCount;
            }
        }
        protected Transform blockParent
        {
            get
            {
                if(_blockParent==null)
                {
                    _blockParent = BlockParent.Instance.transform;
                }
                return _blockParent;
            }
        }
        protected Transform _blockParent;

        protected void Update()
        {
            //if not in menu & winning
            if(!LevelChanger.isMenu)
            {
                //contemplate moving to next level
                if(numGoldWin!=null && goldThisLevel>=numGoldWin)
                {
                    MoveToNextDifficulty();
                }
                if(numBlocksWin!=null && numBlocks<=numBlocksWin)
                {
                    MoveToNextDifficulty();
                }
            }
        }

        //A list of difficulty levels to switch between once a difficulty is beaten
        public List<TextAsset> midLevelDifficulties;
        public int currentMidLevel = 0;

        // Execute the Action when Moving to difficulty level corresponding to dictionary int
        public Dictionary<int, Action> diffBeginActions = new Dictionary<int, Action>();
        public void AddBeginDifficultyAction(int level, Action action)
        {
            if(!diffBeginActions.Keys.Contains(level))
            {
                diffBeginActions.Add(level, action);
            }
        }

        //Get the difficulty stat for currentMidLevel
        //  basically midLevelDifficulties[currentMidLevel], but
        //  offset by milestoneDifficulties[currentLevel]
        protected DifficultyStats GetCurrentDifficultyStats()
        {
            int realLevel = currentMidLevel;// + milestoneDifficulties[currentLevel];
            if(realLevel>=midLevelDifficulties.Count)
            {
                //no more levels
                //Debug.Log(realLevel + "real level too high");
                return null;
            }
            //the difficulty exists, return it
            return JsonConvert.DeserializeObject<DifficultyStats>(midLevelDifficulties[realLevel].text);
        }

        /// <summary>
        /// Swap my list of difficulties out with an outside list
        /// And get started with those difficulties
        /// </summary>
        /// <param name="newDifficulties"></param>
        public void SetDifficultyLevels(List<TextAsset> newDifficulties)
        {
            midLevelDifficulties = newDifficulties;
            MoveToDifficulty(0);

            //clear events
            diffBeginActions = new Dictionary<int, Action>();
        }

        public void RestartDifficulties()
        {
            ScoreManager.Instance.goldVal = 0;
            MoveToDifficulty(0);
        }

        public void MoveToNextDifficulty()
        {
            MoveToDifficulty(currentMidLevel + 1);
        }

        protected void MoveToDifficulty(int level)
        {
            currentMidLevel = level;

            if(diffBeginActions.Keys.Contains(level))
            {
                //execute the extra action for moving to a new difficulty
                diffBeginActions[level]();
            }

            DifficultyStats difficulty = GetCurrentDifficultyStats();
            if(difficulty!=null)
            {
                difficulty.SetGameValues();
            }
            else
            {
                //Debug.Log("out of difficulties, you probably win");
                WinLoseEffects.Instance.Win();
            }
        }
    }
}
