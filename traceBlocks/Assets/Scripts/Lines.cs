﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Vectrosity;

namespace BlockGame
{
    public static class InputMouse
    {
        public static Vector3 worldPosition
        {
            get
            {
                return Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
        }
    }

    public class ILine2D
    {
        public virtual Vector2 start
        {
            get;
            set;
        }
        public virtual Vector2 end
        {
            get;
            set;
        }

        public ILine2D()
        {

        }

        public ILine2D(Vector2 start, Vector2 end)
        {
            this.start = start;
            this.end = end;
        }

        public virtual void Clear()
        {

        }

        public virtual void Draw()
        {

        }
    }

    public class VectrosityLine : ILine2D
    {
        public VectorLine vLine;

        public override Vector2 start
        {
            get
            {
                return vLine.points3[0];
            }
            set
            {
                vLine.points3[0] = value;
            }
        }

        public override Vector2 end
        {
            get
            {
                return vLine.points3[1];
            }
            set
            {
                vLine.points3[1] = value;
            }
        }

        public VectrosityLine(Vector2 start, Vector2 end)
        {
            vLine = VectorLine.SetLine(Color.white, new Vector3(start.x,start.y,0), new Vector3(end.x,end.y,0));
            vLine.lineWidth = 2;
            vLine.sortingLayerID = 0;
        }

        public override void Clear()
        {
            VectorLine.Destroy(ref vLine);
        }

        public override void Draw()
        {
            vLine.Draw();
        }
    }

    /// <summary>
    /// Draws lines between blocks the user has collected
    /// And calls UserInput touch function at positions along the line
    /// Deprecated line drawing, but still use it to call UserInput
    /// </summary>
    public class Lines : MonoBehaviour
    {
        private IList<ILine2D> lines;
        private ILine2D mouseLine = null;
        public static Lines instance = null;

        public UserInput input;

        public bool drawLines = true;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        public bool useLimits = true;
        public float selectDist;

        ///Set how far away from last block placed to call
        ///For selecting blocks and sending messages to UserInput
        public void SetSelectDist(float worldDist)
        {
            Vector3 t = transform.position;
            Vector3 startPos = Camera.main.WorldToScreenPoint(t);
            Vector3 endPos = Camera.main.WorldToScreenPoint(new Vector3(t.x + worldDist, t.y, t.z));
            selectDist = (endPos - startPos).x;
        }

        public void UpdateLine()
        {
            if(mouseLine!=null)
            {
                mouseLine.end = GetPointAtMaxDistance(mouseLine.start, Input.mousePosition, selectDist);
                //Add all the blocks in between cursor and block too
                input.ClickOnLine(mouseLine.start, mouseLine.end);
            }
        }

        //Return a vector along the line from start-end that is either end or at dist, whichever is shorter
        private Vector3 GetPointAtMaxDistance(Vector3 start, Vector3 end, float dist)
        {
            Vector3 direction = end - start;
            if (direction.magnitude > dist)
            {
                direction = direction.normalized * dist;
            }
            return start + direction;
        }

        public  void Initialize(IBlock block)
        {
            if (mouseLine == null)
            {
                Vector2 mPos = (Vector2)Input.mousePosition;
                if(drawLines)
                {
                    mouseLine = new VectrosityLine(mPos, mPos);//Input.mousePosition,Input.mousePosition);
                }
                else
                {
                    mouseLine = new ILine2D(mPos, mPos);
                }
            }
        }

        private void Update()
        {
            UpdateLine();
            Draw();
        }

        public void Draw()
        {
            if(lines!=null)
            {
                foreach (ILine2D line in lines)
                {
                    line.Draw();
                }
            }
        }

        //Clear all lists and destroy all ILine2D
        public  void Clear()
        {
            if (mouseLine != null)
            {
                mouseLine = null;
            }
            if (lines != null)
            {
                foreach(ILine2D line in lines)
                {
                    line.Clear();
                }
                lines = null;
            }
        }

        protected IBlock lastBlock;

        //Add a line connecting v1 & v2
        public  void AddLine(Vector3 v1, Vector3 v2)
        {
            if (lines == null)
                lines = new List<ILine2D>();
            if(drawLines)
            {
                lines.Add(new VectrosityLine(v1, v2));
            }
            else
            {
                lines.Add(new ILine2D(v1, v2));
            }
            if (mouseLine != null)
            {
                mouseLine.start = (Vector2)Camera.main.WorldToScreenPoint(v2);
            }
        }

        //Remove line at position i that corresponds to the block at that position
        public  void RemoveLine(int i)
        {
            //the first (non-mouse) line is drawn when there are 2 blocks
            i = i - 1;
            ILine2D line = lines[i];
            line.Clear();
            lines.RemoveAt(i);
            if (i == lines.Count)
            {
                //we just removed the final piece of the line, so move mouse drawer back
                mouseLine.start = (Vector2)Input.mousePosition;
            }
        }
    }
}