﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class NothingThere : MonoBehaviour
    {
        private WaveSpawner spawner;
        public GameObject fastIndicator;

        public float startingTime = 0.8f;
        private float cautionTicker = 0f;

        void Start()
        {
            spawner = SpawnerObject.Instance.GetOrAddComponent<WaveSpawner>();
        }

        bool heldDown = false;

        public void Click(Vector3 pos)
        {
       /*     cautionTicker += Time.deltaTime;
            heldDown = true;
            
            //providing a buffer zone so people actually have to try to activate falling fast
            if(cautionTicker>=startingTime)
            {
                spawner.SpawnFast();
                if (fastIndicator != null)
                {
                    fastIndicator.SetActive(true);
                    fastIndicator.transform.position = pos;
                }
            }*/
        }

        protected void Update()
        {
            if(heldDown)
            {
                heldDown = false;
            }
            else
            {
                cautionTicker = 0;
                if(fastIndicator!=null)
                {
                    fastIndicator.SetActive(false);
                }
            }
        }
    }
}
