﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    //I want to manage scenes by just changing a difficult file rather than just changing scene
    //So I have to do that work manually
    //Also, Unity doesn't have an "OnSceneExit" function, so the levelIsEnding bool does that too
    public class LevelChanger : Singleton<LevelChanger>
    {
        //Is the game currently in the process of closing out a scene, destroying all gameObjects, etc?
        public static bool levelIsEnding;
        public static int currentLevel;
        public static int numLevels = 3;
        public enum Level { Tutorial = 0, Easy, Hard, Endless }

        public static bool isMenu
        {
            get
            {
                return (Application.loadedLevel == 0);
            }
        }

        /// <summary>
        /// Go to the game "level" with this value
        /// This roughly corresponds to what "scene" they would be otherwise, 
        ///     and maps to a bunch of different difficulty files
        /// </summary>
        /// <param name="level"></param>
        public void ChangeLevel(int level)
        {
            Application.LoadLevel(1);
            currentLevel = level;
            difficultySelector.SetLevel(level);
            levelIsEnding = true;
        }

        public void RestartLevel()
        {
            ChangeLevel(currentLevel);
        }

        protected DifficultySelector difficultySelector;
        public void Awake()
        {
            base.Awake();
            difficultySelector = gameObject.GetOrAddComponent<DifficultySelector>();
            DontDestroyOnLoad(gameObject);
            levelIsEnding = false;
        }

        //Actual scenes should be structured such that 0=menu, 1=game
        public void GoToMenu()
        {
            Application.LoadLevel(0);
            levelIsEnding = true;
        }

        protected void OnLevelWasLoaded(int level)
        {
            //applicationIsQuitting = false;
            levelIsEnding = false;
        }
    }
}
