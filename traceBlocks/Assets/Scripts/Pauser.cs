﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Pauses the game by setting deltaTime to 0
    /// Called for GameOver and by PauseButton to show a menu
    /// </summary>
    public class Pauser : Singleton<Pauser>
    {
        protected void Start()
        {
            priorTimeScale = Time.timeScale;
            paused = false;
        }

        protected float priorTimeScale;
        protected bool paused;

        public Transform pauseMenu;

        public bool isPaused
        {
            get
            {
                return paused;
            }
        }

        public void GameOver()
        {
            PauseTime();
            //Don't let people press pause during game over screen
            PauseButton pauseButton = GetComponent<PauseButton>();
            pauseButton.enabled = false;
        }

        protected void PauseTime()
        {
            if (paused)
            {
                return;
            }
            priorTimeScale = Time.timeScale;
            Time.timeScale = 0;
            paused = true;
        }

        protected void UnPauseTime()
        {
            if (!paused)
            {
                return;
            }
            Time.timeScale = priorTimeScale;
            paused = false;
        }

        public void Pause()
        {
            PauseTime();
            SetMenuActive(paused);

          //  GoogleAnalyticsV3.getInstance().LogScreen("PauseMenu");
        }

        /// <summary>
        /// Release time stop
        ///     and hide the menu options
        /// </summary>
        public void UnPause()
        {
            UnPauseTime();
            TextOnTop.MoveAllTextUp();
            SetMenuActive(paused);
        }

        protected void SetMenuActive(bool active)
        {
            pauseMenu.gameObject.SetActive(active);
/*            foreach(Transform t in pauseMenu)
            {
                t.gameObject.SetActive(active);
            }*/
        }
    }
}
