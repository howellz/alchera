﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Text;

namespace BlockGame
{
    public abstract class Floor : MonoBehaviour
    {
        protected void Start()
        {
            GridManager grid = GridManager.instance;
            //scale so floor matches the grid
            BoxCollider2D floorBox = gameObject.GetComponent<BoxCollider2D>();

            //boxcollider2d size doesn't take into account localScale
            Vector3 newScale;
            newScale = new Vector3(grid.ext.x / ((floorBox.size.x / 2) * transform.localScale.x), 1f, 1f);

            transform.localScale = new Vector3(newScale.x * transform.localScale.x,
                                            newScale.y * transform.localScale.y,
                                            newScale.z * transform.localScale.z);
        }

        public void Show()
        {
            SetShown(true);
        }

        abstract protected void SetShown(bool show);

        public void Hide()
        {
            SetShown(false);
        }
    }
}
