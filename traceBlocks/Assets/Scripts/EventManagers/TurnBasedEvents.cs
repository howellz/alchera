﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class TurnBasedEvents : WaveSpawner
    {
        public void BlocksCombined()
        {
            SpawnWave();
        }
    }
}
