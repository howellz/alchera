﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class WaveSpawner : MonoBehaviour
    {
        public BlockSpawner spawner;

        public bool canSpawn = true;
        public float smallSpawnTime = 0.6f;
        public int numBlocksInWave = 5;
        public int numWavesStart = 5;

        //The number of waves spawned so far in the game, for tracking purposes
        //Starting waves were throwing this off too much, so ignore them
        public int numWavesSpawned 
        {
            get 
            {
                if(_wavesSpawned<numWavesStart)
                {
                    return 0;
                }
                else 
                {
                    return _wavesSpawned - numWavesStart; 
                }
            } 
        }

        protected int _wavesSpawned = 0;

        protected virtual void Start()
        {
            StartingWaves();
        }

        protected void StartingWaves()
        {
            if (canSpawn)
            {
                for (int i = 0; i <= numWavesStart; i++)
                {
                    Invoke("SpawnWave", i * 2.4f);
                }
            }
        }

        /// <summary>
        /// Spawn #numBlocksInWave blocks, each separated by smallSpawnTime time
        /// </summary>
        public virtual void SpawnWave()
        {
            if(spawner==null)
            {
                Debug.Log("spawner not assigned to event manager");
                return;
            }
            _wavesSpawned += 1;
            for(int i=0;i<numBlocksInWave;i++)
            {
                Invoke("SpawnBlock", i * smallSpawnTime);
            }
        }

        protected void SpawnBlock()
        {
            spawner.SpawnBlock();
        }


        /// <summary>
        /// Called by button press on spawn object
        /// </summary>
        public void SpawnFast()
        {
            SpawnWave();
        }
    }
}
