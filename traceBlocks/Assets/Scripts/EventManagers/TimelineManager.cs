﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class TimelineManager : WaveSpawner
    {
        public float time = 0;

        //how often should we spawn blocks
        public float spawnTime = 9f;
        public float minSpawnTime = 3f;

        //how fast should we speed up block spawning?
        public int speedUpTime = 21;
        public float speedUpAmt = 1f;

        void Start()
        {
            base.Start();
        }

        public int curTime = 0;
        public int prevTime = -1;
        protected void Update()
        {
            time += Time.deltaTime;
            curTime = (int) Mathf.Floor(time);
            if(curTime>prevTime)
            {
                //we just advanced a time step
                //actually check triggers
                if(curTime%((int)spawnTime)==0)
                {
                    SpawnWave();
                }
                if(curTime%(int)speedUpTime==0)
                {
                    SpeedUp();
                }
            }
            prevTime = curTime;
        }

        public void SpeedUp()
        {
            if(spawnTime>minSpawnTime)
            {
                spawnTime -= speedUpAmt;
            }
        }
    }
}
