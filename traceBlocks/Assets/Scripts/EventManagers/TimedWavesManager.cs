﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using ZachUtility;

namespace BlockGame
{
    /// <summary>
    /// Spawn a IBlock Wave every spawnTime time
    ///     Very simple - doesn't speed up or do anything except spawn block waves
    /// </summary>
    public class TimedWavesManager : WaveSpawner
    {
        protected float time = 0;

        //how often should we spawn blocks
        public float spawnTime = 9f;

        void Start()
        {
            //base.Start();
            StartingWaves();
            waveTimer = new Timer(spawnTime);
        }

        protected Timer waveTimer;

        public override void SpawnWave()
        {
            //spawn a wave and reset spawn time of next wave
            waveTimer = new Timer(spawnTime);
            base.SpawnWave();
        }

        protected void Update()
        {
            if(waveTimer.Update())
            {
                SpawnWave();
            }
        }
    }
}
