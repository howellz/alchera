﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using ZachUtility;

namespace BlockGame
{
    /// <summary>
    /// Cue the SpeedUpManager to speed up the waves
    /// Also keep track of how many times we've sped up
    /// </summary>
    public class SpeedUpTrigger : MonoBehaviour
    {
        //How will speed ups be triggered?  Two nullable options:
        //  trigger when a certain number of waves have been called
        protected int? numWavesSpeedUp = 3;
        //  trigger after a set amount of time
        protected float? speedUpTime = null;
        protected Timer speedUpTimer;

        public void SetNumWavesSpeedUp(int? numWaves)
        {
            numWavesSpeedUp = numWaves;
            if(numWaves!=null)
            {
                //kill the alt approach
                speedUpTime = null;
                speedUpTimer = null;
            }
        }

        public void SetSpeedUpTime(float? time)
        {
            speedUpTime = time;
            if(time!=null)
            {
                speedUpTimer = new Timer(time ?? 1f);
                speedUpTimer.Done = delegate()
                {
                    speedUpTimer.ReStart();
                    SpeedUp();
                };
                //kill alt
                numWavesSpeedUp = null;
            }
        }

        protected SpeedUpManager speedUp;
        protected WaveSpawner spawner;
        protected DifficultyIncreaser difficultyIncreaser;

        protected void Start()
        {
            if(speedUp==null)
            {
                speedUp = gameObject.GetComponent<SpeedUpManager>();
            }
            if(spawner==null)
            {
                spawner = gameObject.GetComponent<WaveSpawner>();
            }
            difficultyIncreaser = DifficultyToolbox.Instance.GetOrAddComponent<DifficultyIncreaser>();
        }

        protected int prevWavesSpawned = 0;

        protected void Update()
        {
            if(numWavesSpeedUp!=null)
            {
                //check to ensure this if statement is only entered once during update cycles of each wave
                int numWaves = spawner.numWavesSpawned;
                if (numWaves != prevWavesSpawned && numWaves % numWavesSpeedUp == 0)
                {
                    SpeedUp();
                }
                prevWavesSpawned = spawner.numWavesSpawned;
            }
            else if(speedUpTime!=null && speedUpTimer!=null)
            {
                speedUpTimer.Update();
            }
            if(difficultyTime!=null && difficultyTimer!=null)
            {
                difficultyTimer.Update();
            }
        }

        //When should the difficulty move to next?
        //number of speed ups before difficulty is increased
        public int? numSpeedUpsDiff = null;
        //number of times the game has been sped up
        protected int totalSpeedUps = 0;
        //Time before difficulty is increased
        protected float? difficultyTime;
        protected Timer difficultyTimer;

        public void SetNumSpeedsUpDiff(int? numSpeedUps)
        {
            numSpeedUpsDiff = numSpeedUps;
            if(numSpeedUps!=null)
            {
                //kill alt method
                difficultyTime = null;
                difficultyTimer = null;
            }
        }

        public void SetDifficultyTime(float? time)
        {
            difficultyTime = time;
            if(time!=null)
            {
                //kill the alt method
                numSpeedUpsDiff = null;
                difficultyTimer = new Timer(difficultyTime ?? 1f);
                difficultyTimer.Done = delegate()
                {
                    difficultyTimer.ReStart();
                    difficultyIncreaser.MoveToNextDifficulty();
                };
            }
        }

        protected void SpeedUp()
        {
            totalSpeedUps++;
            //have we sped up enough times to move to next difficulty?
            //This goes before speedUp.SpeedUp so that beginMessage is created before speedUpMessage.  kinda hacky...
            if (numSpeedUpsDiff != null && totalSpeedUps >= numSpeedUpsDiff)
            {
                difficultyIncreaser.MoveToNextDifficulty();
                totalSpeedUps = 0;
            }
            speedUp.SpeedUp();
        }

    }
}
