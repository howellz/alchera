﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using ZachUtility;

namespace BlockGame
{
    /// <summary>
    /// Cue the TimedWavesManager to spawn block waves more frequently
    /// </summary>
    public class SpeedUpManager : MonoBehaviour
    {
        protected TimedWavesManager waveSpawner;
    
        public float minSpawnTime = 4f;
        public float startSpawnTime
        {
            get
            {
                return _startSpawnTime;
            }
            set
            {
                _startSpawnTime = value * hardFactor;
                waveSpawner.spawnTime = _startSpawnTime;
            }
        }
        private float _startSpawnTime = 5f;

        public float speedUpAmt = 1f;

        //set all the important values
        public void SetTimes(float? minSpawnTime, float? startSpawnTime, float? speedUpAmt)
        {
            if(minSpawnTime!=null)
            {
                minSpawnTime = minSpawnTime ?? this.minSpawnTime;
                this.minSpawnTime *= this.hardFactor;
            }
            if(startSpawnTime!=null)
            {
                startSpawnTime = startSpawnTime ?? this.startSpawnTime;
            }
            if(speedUpAmt!=null)
            {
                speedUpAmt = speedUpAmt ?? this.speedUpAmt;
                this.speedUpAmt *= this.hardFactor;
            }
        }

        public void SetHardFactor(float newHardFactor)
        {
            hardFactor = newHardFactor;
            startSpawnTime *= hardFactor;
            minSpawnTime *= hardFactor;
            speedUpAmt *= hardFactor;
        }

        public float hardFactor = 1f;

        public bool spawnSpeedUpMsgs = false;

        protected void Awake()
        {
            if(waveSpawner==null)
            {
                waveSpawner = gameObject.GetComponent<TimedWavesManager>();
                waveSpawner.spawnTime = startSpawnTime;
            }
        }

        protected void Start()
        {
            messager = Toolbox.Instance.GetComponent<MessageFactory>();
        }

        protected MessageFactory messager;
        public string defaultMessage = "Speed Up";

        public void SpeedUp()
        {
            //did we actually speed up or are we at max?
            bool spedUp = false;
            if (waveSpawner.spawnTime > minSpawnTime)
            {
                waveSpawner.spawnTime -= speedUpAmt;
                //Check if we went too far
                if(waveSpawner.spawnTime < minSpawnTime)
                {
                    waveSpawner.spawnTime = minSpawnTime;
                }
                else
                {
                    spedUp = true;
                }
            }

            //Display messages for user
            if (spedUp && spawnSpeedUpMsgs)
            {
                messager.WriteMessage(defaultMessage);
            }
        }

    }
}
