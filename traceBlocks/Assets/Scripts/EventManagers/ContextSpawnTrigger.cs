﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using ZachUtility;

namespace BlockGame
{
    //Attach this to the "AllBlocks" object that all blocks are parented under
    //Reference this with BlockParent.Instance.GetComponent
    public class ContextSpawnTrigger : MonoBehaviour
    {
        //spawn more blocks if fewer than this number of blocks on screen
        public int minBlocksOnScreen = 21;
        //how often to check if there are too few blocks
        protected float checkTime = 1.0f;
        protected Timer checkTimer;

        protected WaveSpawner spawner;

        public int numBlocks
        {
            get
            {
                return transform.childCount;
            }
        }

        protected void Start()
        {
            spawner = SpawnerObject.Instance.GetOrAddComponent<TimedWavesManager>();
            checkTimer = new Timer(checkTime);
            checkTimer.Done = delegate() { 
                checkTimer.ReStart();
                if (numBlocks < minBlocksOnScreen)
                {
                    //spawn more blocks!
                    spawner.SpawnWave();
                }
            };
        }
        protected void Update()
        {
            checkTimer.Update();
        }
    }
}
