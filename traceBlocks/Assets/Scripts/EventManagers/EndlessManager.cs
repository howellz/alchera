﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Manages starting in Endless mode 
    /// & playing through the difficulties again and again
    /// 
    /// Multiple possible approaches:
    ///     Play through, show menu with Easy++ 
    ///         to represent how many times you've beaten it in a row
    ///     Start in Endless mode, then reset before menu
    ///     
    ///     When reset, either restart scene or reset difficulties
    ///     Currently it resets difficulties (not scene) and doesn't show menu
    /// </summary>
    public class EndlessManager : MonoBehaviour
    {
        protected SpeedUpManager speedUpper
        {
            get
            {
                if(_speedUpper==null)
                {
                    _speedUpper = SpawnerObject.Instance.GetOrAddComponent<SpeedUpManager>();
                }
                return _speedUpper;
            }
        }
        private SpeedUpManager _speedUpper;

        protected DifficultyIncreaser diffIncreaser
        {
            get
            {
                if (_diffIncreaser == null)
                {
                    _diffIncreaser = DifficultyToolbox.Instance.GetOrAddComponent<DifficultyIncreaser>();
                }
                return _diffIncreaser;
            }
        }
        private DifficultyIncreaser _diffIncreaser;

        public float hardFactor = 0.96f;
        public int numEndless = 0;
        protected bool justRestarted = false;
        protected int prevScore = 0;

        public void IncreaseEndless()
        {
            numEndless += 1;
            speedUpper.SetHardFactor(Mathf.Pow(hardFactor, numEndless));
        }

        /// <summary>
        /// At the start of each level,
        ///     determine if we got there legimitately
        ///     And set up the end game
        /// </summary>
        /// <param name="level"></param>
        protected void OnLevelWasLoaded(int level)
        {
            if(!LevelChanger.isMenu)
            {
                WinLoseEffects winLose = WinLoseEffects.Instance;
                if(numEndless>1)
                {
                    //we restarted in endless mode, so make it easier again
                    numEndless = 1;
                }
                //make faster and record previous score
                speedUpper.SetHardFactor(Mathf.Pow(hardFactor, numEndless));
                ScoreManager.Instance.scoreVal = prevScore;
            }
            else
            {
                //we went back to the menu, reset
                FailLevel();
            }
        }

        /// <summary>
        /// Returns whether win screen can be displayed
        ///     Side effect:
        ///     If not, sets up the next stage
        /// </summary>
        /// <returns></returns>
        public bool TryToWin()
        {
            if(numEndless==0)
            {
                IncreaseEndless();
                return true;
            }
            //we're in endless mode!
            //so just restart the difficulties but keep going
            IncreaseEndless();
            diffIncreaser.RestartDifficulties();
            return false;
        }

        /// <summary>
        /// Entered level not through winning, restart
        /// </summary>
        public void FailLevel()
        {
            numEndless = 0;
            justRestarted = false;
            prevScore = 0;
        }

        protected void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                StartEndless();
            }
        }

        /// <summary>
        /// Restart the level, new level is in endless mode
        /// </summary>
        public static void StartEndless()
        {
            Pauser.Instance.UnPause();
            LevelChanger.Instance.RestartLevel();
            EndlessManager endlessManager = DifficultyToolbox.Instance.GetComponent<EndlessManager>();
            endlessManager.justRestarted = true;
            endlessManager.prevScore = ScoreManager.Instance.scoreVal;
        }

        //Just get endless mode going for the scene
        public void TurnEndlessOn()
        {
            IncreaseEndless();
        }

    }
}
