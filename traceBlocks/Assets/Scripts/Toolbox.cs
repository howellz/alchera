﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class Toolbox : Singleton<Toolbox>
    {
        //protected Toolbox() { } // guarantee this will be always a singleton only - can't use the constructor!  //Does this work in Unity?

        public string myGlobalVar = "whatever";

        public override void Awake()
        {
            // Your initialization code here
            base.Awake();
        }

        // (optional) allow runtime registration of global objects
        static public T RegisterComponent<T>() where T : Component
        {
            return Instance.GetOrAddComponent<T>();
        }
    }
}
