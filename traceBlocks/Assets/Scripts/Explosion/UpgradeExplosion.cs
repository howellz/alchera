﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class UpgradeExplosion : IExplosion
    {
        protected BlockFactory factory;
        protected BlockUpgrader upgrader;

        void Start()
        {
            factory = Toolbox.Instance.GetComponent<BlockFactory>() as BlockFactory;
            upgrader = (BlockUpgrader)Toolbox.Instance.GetOrAddComponent<BlockUpgrader>();
            base.Start();
        }
        
        public override void Explode(IEnumerable<GridVector> positions)
        {
            foreach(GridVector ijPos in positions)
            {
                IEnumerable<RaycastHit2D> hits = grid.GetHitsAtij(ijPos);
                Debug.Log("upgrading " + hits.Count() + " num blocks");
                foreach (RaycastHit2D hit in hits)
                {
                    IBlock block = hit.transform.gameObject.GetComponent<IBlock>();
                    if(block!=null)
                    {
                        CreateRandomizedBlock(block);
                    }
                    //hit.transform.gameObject.SendMessage("PoliteKill", null, SendMessageOptions.DontRequireReceiver);
                }
                //if (explosionPrefab != null)
                //    GameObject.Instantiate(explosionPrefab, grid.ijToxyz(ijPos), Quaternion.identity);
            }
        }

        public float rainbowChance = 0.05f;

        protected void CreateRandomizedBlock(IBlock block)
        {
            //Create a rainbow block with a certain chance or if the block is too high to be upgraded
            //Otherwise, create an upgraded block
            if(!upgrader.CreateUpgradeBlock(block))//UnityEngine.Random.Range(0f,1f)<=rainbowChance || !upgrader.CreateUpgradeBlock(block))
            {
                //Create a rainbow block
                factory.CreateBlock(BlockColor.rainbowColor, block.transform.position);
            }
            block.PoliteKill();
        }
    }
}
