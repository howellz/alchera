﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public abstract class IExplosion : MonoBehaviour
    {
        protected GridManager grid;

        public void Start()
        {
            grid = GridManager.instance;
        }

        public abstract void Explode(IEnumerable<GridVector> positions);
    }
}
