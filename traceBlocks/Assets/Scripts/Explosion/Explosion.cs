﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class Explosion : IExplosion
    {
        public GameObject explosionPrefab;
        
        public override void Explode(IEnumerable<GridVector> positions)
        {
            foreach(GridVector ijPos in positions)
            {
                IEnumerable<RaycastHit2D> hits = grid.GetHitsAtij(ijPos);
                foreach (RaycastHit2D hit in hits)
                {
                    hit.transform.gameObject.SendMessage("PoliteKill", null, SendMessageOptions.DontRequireReceiver);
                }
                if (explosionPrefab != null)
                    GameObject.Instantiate(explosionPrefab, grid.ijToxyz(ijPos), Quaternion.identity);
            }
        }
    }
}
