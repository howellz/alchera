﻿using UnityEngine;

namespace BlockGame
{ 
    static public class MethodExtensionForMonoBehaviour
    {
        /// <summary>
        /// Gets or add a component. Usage example:
        /// BoxCollider boxCollider = transform.GetOrAddComponent<BoxCollider>();
        /// </summary>
        static public T GetOrAddComponent<T>(this Component child) where T : Component
        {
            T result = child.gameObject.GetOrAddComponent<T>();
            return result;
        }

        static public T GetOrAddComponent<T>(this GameObject g) where T : Component
        {
            T result = g.GetComponent<T>();
            if (result == null)
            {
                result = g.AddComponent<T>();
            }
            return result;            
        }
    }

}