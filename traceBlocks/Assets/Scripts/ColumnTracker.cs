﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Keeps track of where the highest block in each row is
    /// </summary>
    public class ColumnTracker : MonoBehaviour
    {
        private GridManager grid;

        int minRow = 1;

        void Start()
        {
            grid = GridManager.instance;
            //initialize the columns
            for(int i=0;i<grid.numColumns;i++)
            {
                columnHeights.Add(minRow);
            }
        }

        public List<int> columnHeights;

        int checkTime = 10;
        int time = 0;

        void Update()
        {
            if(time%checkTime==0)
                UpdateHeights();
            time++;
        }

        void UpdateHeights()
        {
            for(int i=0; i<columnHeights.Count;i++)
            {
//                Debug.Log("i:"+i);
                int height = columnHeights[i];
                bool blockAtHeight = grid.IsTagAtPos(grid.ijToxyz(new GridVector(i,height)),"Block");
                if(blockAtHeight)
                {
                    while (blockAtHeight && height <= grid.numRows)
                    {
                        //set to current height, check next highest
                        columnHeights[i] = height;
                        height++;
                        blockAtHeight = grid.IsTagAtPos(grid.ijToxyz(new GridVector(i, height)), "Block");
                    }
                }
                else
                {
                    //check going down.=
                    while (!blockAtHeight && height >= minRow)
                    {
                    //    Debug.Log("going down"+height+"i"+i);
                        //set to current height, check next highest
                        columnHeights[i] = height;
                        height--;
                        blockAtHeight = grid.IsTagAtPos(grid.ijToxyz(new GridVector(i, height)), "Block");
                    }
                }
            }
        }

        public bool IsColumnWarning(int col)
        {
            return (columnHeights[col] >= (grid.numRows - 1));
        }

        public IEnumerable<int> FindBlockColumnsInRow(int row)
        {
            List<int> columns = new List<int>();
            int i = 0;
            for (GridVector v = new GridVector(0, row); v.x < grid.numColumns; v += new GridVector(1, 0))
            {
                if (grid.IsTagAtPos(grid.ijToxyz(v), "Block"))
                {
                    columns.Add(i);
                }
                i++;
            }
            return columns;
        }
    }
}
