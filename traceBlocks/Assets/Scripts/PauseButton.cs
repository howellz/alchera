﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class PauseButton : MonoBehaviour
    {
        protected Pauser pauser;
        
        protected void Start()
        {
            pauser = gameObject.GetComponent<Pauser>();
        }

        protected void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                pauser.Pause();
            }
        }

        public void OnClick()
        {
            //disabling the component should disable the pressing in this case
            if(!enabled)
            {
                return;
            }
            pauser.Pause();
        }
    }
}
