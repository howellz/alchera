﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Place this on a 3DText object to make it show up above GUI 
    /// </summary>
    //Revise thsi so that it uses more layers rather than bad practice
    public class TextOnTop : MonoBehaviour
    {
        protected void Start()
        {
            MoveUp();
        }

        //Stop this text from floating above GUI sprites anymore
        public void MoveDown()
        {
            renderer.sortingLayerID = 0;
        }

        public void MoveUp()
        {
            renderer.sortingLayerID = 1;
        }

        public static void MoveAllTextDown()
        {
            //hide all the current text
            TextOnTop[] texts = GameObject.FindObjectsOfType<TextOnTop>();
            foreach (TextOnTop text in texts)
            {
                //doesn't return disabled objects (the menu text)
                text.MoveDown();
            }
        }

        public static void MoveAllTextUp()
        {
            //show all the current text
            TextOnTop[] texts = GameObject.FindObjectsOfType<TextOnTop>();
            foreach (TextOnTop text in texts)
            {
                //doesn't affect disabled objects (the menu text)
                text.MoveUp();
            }
        }
    }
}
