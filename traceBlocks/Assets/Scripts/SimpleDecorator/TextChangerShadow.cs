﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Put on a TextMesh object that has a "shadow" text object that says the same thing
    /// Can then change text of both objects by calling ChangeText
    /// </summary>
    public class TextChangerShadow : MonoBehaviour
    {
        public TextMesh _me;
        public TextMesh _child;

        protected TextMesh me
        {
            get
            {
                if(_me==null)
                {
                    _me = gameObject.GetComponent<TextMesh>();
                }
                return _me;
            }
        }
        public TextMesh child
        {
            get
            {
                if (_child == null)
                {
                    _child = transform.GetChild(0).GetComponent<TextMesh>();
                }
                return _child;
            }
        }

        public void ChangeText(string newText)
        {
            me.text = newText;
            child.text = newText;
        }
    }
}
