﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Simply puts the attached object/renderer in the specified sorting layer
    /// </summary>
    public class SortingLayerSetter : MonoBehaviour
    {
        public int sortingLayerId = -1;
        public string sortingLayerName = "";

        protected void Start()
        {
            if(sortingLayerName!="")
            {
                renderer.sortingLayerName = sortingLayerName;
            }
            else if (sortingLayerId != -1)
            {
                renderer.sortingLayerID = sortingLayerId;
            }

        }
    }
}
