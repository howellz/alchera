﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using ZachUtility;

namespace BlockGame
{
    public class BlinkingSprite : MonoBehaviour
    {
        public float blinkTime = 0.2f;
        protected Timer blinkTimer;
        protected bool on = true;

        protected void Start()
        {
            blinkTimer = new Timer(blinkTime);
            blinkTimer.Done = delegate
            {
                on = !on;
                renderer.enabled = on;
                blinkTimer.ReStart();
            };
        }

        protected void Update()
        {
            blinkTimer.Update();
        }
    }
}
