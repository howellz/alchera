﻿using UnityEngine;
using System.Collections;

public class ColorChangeOverTime : MonoBehaviour {

	private Renderer thisRenderer;

	public Color colorStart = Color.red;
	public Color colorEnd = Color.blue;
	public Color colorReturn = new Color32 (124, 199, 249, 255);
	public float duration = 1.0F;
	public bool warningOn = false;

	// Use this for initialization
	void Start () {
		thisRenderer = this.renderer;
	}
	
	// Update is called once per frame
	void Update () {
		if (warningOn) 
		{
			float lerp = Mathf.PingPong (Time.time, duration) / duration;
			thisRenderer.material.color = Color.Lerp (colorStart, colorEnd, lerp);
		}
	}

	public void TurnOn()
	{
		warningOn = true;
	}

	public void TurnOff()
	{
		warningOn = false;
		thisRenderer.material.color = colorReturn;
	}

    public void TurnFullAndOff()
    {
        warningOn = false;
        thisRenderer.material.color = colorEnd;
    }
}
