﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Gameobject attached will unparent all of its children
    /// And then kill itself
    /// </summary>
    public class ChildDistributor : MonoBehaviour
    {
        protected void Start()
        {
            //have to iterate through backwards because removing elements
            for (int i = transform.childCount - 1; i >= 0;i-=1 )
            {
                transform.GetChild(i).parent = transform.parent;
            }
            GameObject.Destroy(gameObject);
        }
    }
}
