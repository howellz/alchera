﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Sends messages across the screen
    /// </summary>
    public class MessageFactory : MonoBehaviour
    {
        //A prefab to spawn (they should destroy themselves)
        public TextMesh speedUpMessage;
        //A transform in the scene of where to spawn them
        public Transform speedUpPos;
        //The currently spawned TextMesh
        protected TextMesh spawnedText;

        //Can store one string to spawn after the previous text has finished
        protected string textToSpawnLater;

        public void WriteMessage(string text)
        {
            if(spawnedText==null)
            {
                spawnedText = (GameObject.Instantiate(speedUpMessage, speedUpPos.position, Quaternion.identity) as TextMesh);
                spawnedText.GetComponent<TextChangerShadow>().ChangeText(text);
            }
            else
            {
                //A spawned text object already exists, don't spawn right now
                textToSpawnLater = text;
            }
        }

        protected void Update()
        {
            if(textToSpawnLater!=null && spawnedText==null)
            {
                //Try to write the message after the previous text is gone
                WriteMessage(textToSpawnLater);
                textToSpawnLater = null;
            }
        }

    }
}
