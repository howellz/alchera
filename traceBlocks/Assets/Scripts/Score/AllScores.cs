﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Just a list of scores to be stored in json
    /// </summary>
    public class AllScores
    {
        public Dictionary<int, LevelScore> scoresForLevels;

        public AllScores()
        {
            scoresForLevels = new Dictionary<int, LevelScore>();
        }

        public void SetScoreLevelSafely(int level, LevelScore lScore)
        {
            if(ContainsLevel(level))
            {
                SetScoreForLevel(level, lScore);
            }
            else
            {
                scoresForLevels.Add(level, lScore);
            }
        }

        public void SetScoreForLevel(int level, LevelScore lScore)
        {
            scoresForLevels[level] = lScore;
        }

        public bool ContainsLevel(int level)
        {
            return scoresForLevels.Keys.Contains(level);
        }

        public LevelScore GetScoreForLevel(int level)
        {
            return scoresForLevels[level];
        }
    }
}
