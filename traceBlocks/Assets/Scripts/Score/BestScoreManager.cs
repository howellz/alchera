﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Record the score to a file and read it back when game next loaded
    /// Save scores when a difficulty is completed, if that score is higher than previous
    /// </summary>
    public class BestScoreManager : MonoBehaviour
    {
        protected DifficultyIncreaser diffIncreaser;
        protected FileInput fileIO;
        public AllScores bestScores;
        public string fName = "scores";
        
        protected void Start()
        {
            fileIO = FileInput.Instance;
            bestScores = fileIO.ReadAndDeserialize<AllScores>(fName).FirstOrDefault();
            diffIncreaser = DifficultyToolbox.Instance.GetOrAddComponent<DifficultyIncreaser>();

            if (bestScores == null)
            {
                bestScores = new AllScores();
                for(int i=0;i<LevelChanger.numLevels;i++)
                {
                    bestScores.SetScoreForLevel(i, new LevelScore());
                }
                SaveAllScores();
            }
            else
            {
                //still check to make sure it's the correct size
                for(int i=0;i<LevelChanger.numLevels;i++)
                {
                    if(!bestScores.ContainsLevel(i))
                    {
                        bestScores.SetScoreForLevel(i, new LevelScore());
                    }
                }
            }

            //WinLoseEffects winLose = WinLoseEffects.Instance;
            //winLose.gameOverEvents.Add(SaveBestScore);
            //winLose.winEvents.Add(SaveBestScore);
        }

        public void SaveBestScore(int i)
        {
            //Get the old and current score for this level
            LevelScore curScore = LevelScore.GetCurrentLevelScore();
            LevelScore oldScore;
            if(bestScores.ContainsLevel(i))
            {
                oldScore = bestScores.GetScoreForLevel(i);
            }
            else
            {
                //score hasn't been initialized yet
                oldScore = new LevelScore();
                bestScores.SetScoreLevelSafely(i, oldScore);
            }

            //Get the best of old and current score
            LevelScore bestScore = oldScore.CombineWith(curScore);
            //Save that score for this level (easy/hard)
            bestScores.SetScoreForLevel(i, bestScore);
            SaveAllScores();
            WinLoseEffects.Instance.highScore = bestScore.score;
        }

        protected void SaveAllScores()
        {
            fileIO.WriteAndSerialize<AllScores>(new List<AllScores>()
            {
                bestScores
            },fName);
        }
    }
}
