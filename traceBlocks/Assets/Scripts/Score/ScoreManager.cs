﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// A container and singleton for gold and regular score values
    /// Makes it quite convenient to add to score and gold values
    /// </summary>
    public class ScoreManager : Singleton<ScoreManager>
    {
        public Score score;
        public int scoreVal
        {
            get
            {
                return score.score;
            }
            set
            {
                score.SetScore(value);
            }
        }
        public Score GetScore()
        {
            return score;
        }
        public Score gold;

        public int goldVal
        {
            get
            {
                return gold.score;
            }
            set
            {
                gold.SetScore(value);
            }
        }
    }
}
