﻿using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Used to be score
    /// Now morphed into a TextMesh container for an integer
    /// </summary>
    public class Score : MonoBehaviour
    {
        protected void Awake()
        {
            //refresh image
            SetScore(score);
        }

        public TextMesh textObject;

        //Text to append to the end of the score integer
        public string appendedText
        {
            get
            {
                return _appendedText;
            }
            set
            {
                _appendedText = value;
                //refresh the score to redraw the appended text
                SetScore(score);
            }
        }
        private string _appendedText = "";

        public int score
        {
            get
            {
                return _score;
            }
            protected set
            {
                _score = value;
                textObject.text = score.ToString() + appendedText;
            }
        }
        private int _score;

        public void AddToScore(int addScore)
        {
            if (addScore < 0)
                return;
            score += addScore;
        }
        public void SetScore(int newScore)
        {
            score = newScore;
        }
    }
}

