﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// scores for json storage
    /// </summary>
    public class LevelScore
    {
        public int gold {get; set;}
        public int score { get; set; }

        public LevelScore()
        {
            gold = 0;
            score = 0;
        }

        /// <summary>
        /// Take the best gold/score of each level and return the score with best stats of each
        /// </summary>
        /// <param name="other"></param>
        public LevelScore CombineWith(LevelScore other)
        {
            LevelScore newScore = new LevelScore();
            newScore.gold = Math.Max(gold, other.gold);
            newScore.score = Math.Max(score, other.score);
            return newScore;
        }

        public static LevelScore GetCurrentLevelScore()
        {
            ScoreManager scoreManager = ScoreManager.Instance;
            LevelScore lScore = new LevelScore
            {
                gold = scoreManager.goldVal,
                score = scoreManager.scoreVal
            };
            return lScore;
        }
    }
}
