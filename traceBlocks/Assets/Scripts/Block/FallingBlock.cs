using UnityEngine;
using System.Collections;

namespace BlockGame
{
    //keep block/block script from interacting with the world until it lands (ie hits something)
    public class FallingBlock : MonoBehaviour
    {
        public IBlock blockScript;

        void Start()
        {
            if (blockScript == null)
            {
                blockScript = gameObject.GetComponent<IBlock>();
            }
            blockScript.enabled = false;
        }

        public void OnCollisionEnter2D(Collision2D col)
        {
            foreach(ContactPoint2D contact in col.contacts)
            {
                //did we hit going down?
                //And did we hit when bottom one has tag block (not in midair)
                if (contact.normal == Vector2.up)
                {
                    if (contact.collider.CompareTag("Floor") || contact.collider.CompareTag("Block"))
                    {
                        //set tag to IBlock here because things that check for "Block" don't really need to know about falling blocks
                        blockScript.enabled = true;
                        gameObject.tag = "Block";
                    }
                    else
                    {
                        //we hit something going down but it wasn't a block?
                        //push this block back up so it can try to hit again (most likely it hit another block that was still falling)
                        rigidbody2D.AddForce(Vector2.up);
                    }
                }
            }
        }
    }

}