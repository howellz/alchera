﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// One method class - mainly wrapper for BlockFactory
    /// Takes a block and "turns it into" the next level of block
    ///     By creating a new block of better color at same position
    /// </summary>
    public class BlockUpgrader : MonoBehaviour
    {
        protected BlockFactory factory;

        void Start()
        {
            factory = Toolbox.Instance.GetComponent<BlockFactory>() as BlockFactory;
            soundPlayer = gameObject.GetComponent<AudioSource>();
        }

        public List<AudioClip> upgradeSounds = new List<AudioClip>();
        protected AudioSource soundPlayer;

        /// <summary>
        /// Take a block and create an upgraded block at the same position
        /// Returns whether block is already of the highest level (gold or rainbow)
        /// </summary>
        /// <param name="block">IBlock script of block to upgrade</param>
        /// <returns>whether block is already of the highest level (gold or rainbow)</returns>
        public bool CreateUpgradeBlock(IBlock block)
        {
            BlockColor blockColor = block.blockColor;
            if (blockColor.CanAdvanceToNextLevel())
            {
                //There's still room to advance
                //Create the object and run a particle effect
                GameObject newBlock = factory.CreateBlock(factory.GetNextColor(blockColor),block.transform.position);//(GameObject)GameObject.Instantiate(blockPrefab, block.transform.position, Quaternion.identity);
                newBlock.GetComponent<Block>().ShowCreatedEffect();

                //Also play an audio source
                //We would make block do this, but we only need one sound for all the combos, so this is probably faster
                if(blockColor.value<upgradeSounds.Count && soundPlayer!=null)
                {
                    soundPlayer.PlayOneShot(upgradeSounds[blockColor.value]);
                }
                return true;
            }
            return false;
        }
    }
}
