﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Vectrosity;

namespace BlockGame
{
    /// <summary>
    /// Manages grabbing blocks, releasing them, and calling other scripts to combine them
    /// Functionality is called by Block (which calls SelectBlock with itself as a parameter) and UserInput (which calls the corresponding method in Block)
    /// </summary>
    public class BlockSelection : MonoBehaviour
    {
        //singleton pattern
        public static BlockSelection Instance
        {
            get
            {
                return _instance;
            }
        }
        private static BlockSelection _instance = null;

        private IList<IBlock> blocks;
        private Lines lines;
        protected BlockSelectGraphics graphics;

        protected void Awake()
        {
            if (Instance == null)
                _instance = this;
            else
                GameObject.Destroy(gameObject);

            blocks = new List<IBlock>();
        }

        protected void Start()
        { 
            //Set up references to other scripts
            lines = Lines.instance;
            selectDist = GridManager.instance.xGridSize * 1.3f;
            lines.SetSelectDist(selectDist);
            graphics = gameObject.GetComponent<BlockSelectGraphics>();

            //Release blocks at end of game
            WinLoseEffects.Instance.winEvents.Add(EndGame);
            WinLoseEffects.Instance.gameOverEvents.Add(EndGame);
        }

        /// Called as action at end of game
        protected void EndGame(int level)
        {
            ReleaseBlocks();
        }

        private BlockColor curColor;
        private float selectDist;

        /// <summary>
        /// Select all of the below blocks (if they're close enough, right color, etc)
        /// And add them to the model
        /// </summary>
        /// <param name="newBlockGameObjects"></param>
        /// <returns></returns>
        private bool TryToSelectBlocks(IEnumerable<GameObject> newBlockGameObjects)
        {
            IEnumerable<IBlock> newBlocks = newBlockGameObjects.Select(g => g.GetComponent<IBlock>());
            if (blocks.Any())
            {
                //Don't add blocks of the wrong color
                BlockColor otherColor = newBlocks.FirstOrDefault().GetComponent<IBlock>().blockColor;
                if(curColor.value == BlockColor.rainbowColor)
                {
                    //choose color on 2nd block for rainbows
                    curColor = otherColor;
                }
                else if (!otherColor.IsEqual(curColor))
                    return false;

                //Don't add blocks that are too far away from the current block
                bool closeEnough = false;
                Vector3 goalPos = blocks.Last().transform.position;
                foreach (IBlock block in newBlocks)
                {
                    if(  (goalPos - block.transform.position)
                                                    .magnitude <= selectDist)  //are any selected blocks close to next block?
                        closeEnough = true;
                }
                if (!closeEnough)
                    return false;

                //Don't add blocks already in the collection
                if (newBlocks.Intersect(blocks).Any())
                {
                    //deselect blocks if user goes backwards
                    if (!newBlocks.Contains(blocks.Last()))
                    {
                        RemoveBlock(blocks.Last());
                    }
                    return false;
                }

                foreach (IBlock block in newBlocks)
                {
                    AddBlock(block);
                }
                return true;
            }
            else
            {
                //no blocks selected yet, set this one as first
                curColor = newBlocks.FirstOrDefault().GetComponent<IBlock>().blockColor;
                foreach (IBlock block in newBlocks)
                {
                    AddBlock(block);
                }
                return true;
            }
        }

        public int maxBlocks = 4;
        public int bonusBlocks = 4;

        public bool SelectBlocks(IEnumerable<GameObject> newBlocks)
        {
            bool success = TryToSelectBlocks(newBlocks);
            if (success)
            {
                if (blocks.Count >= maxBlocks)
                {
                    //We're done, combine the blocks!
                    DoneWithBlocks();
                }
            }
            return success;
        }

        //return whether actually added or not
        public bool SelectBlock(GameObject block)
        {
            //just use the code for a list of one
            return SelectBlocks(new List<GameObject> { block });
        }

        private void AddBlock(IBlock block)
        {
            if (blocks.Any())
            {
                lines.AddLine(blocks.Last().transform.position, block.transform.position);
                graphics.AddBlock(block);
            }
            else
            {
                lines.Initialize(block);
                graphics.Initialize(block);
            }
            block.GetComponent<IBlock>().GetGrabbed();
            blocks.Add(block);
        }

        private void RemoveBlock(IBlock block)
        {
            //release the block
            block.GetComponent<IBlock>().Release();
            int blockIndex = blocks.IndexOf(block);
            blocks.RemoveAt(blockIndex);
            graphics.RemoveBlock(blockIndex);
            lines.RemoveLine(blockIndex);
        }


        protected void Update()
        {
            if(Input.GetMouseButtonUp(0) && blocks.Count>0)
            {
                DoneWithBlocks();
            }
        }

        public void DoneWithBlocks()
        {
            lines.Clear();
            graphics.Clear();
            //release blocks back if not enough (only 1) or attempt to combine them
            if (blocks.Count > 1)
            {
                CombineBlocks();
            }
            else
            {
                //not enough blocks to form a collider, just release them back
                ReleaseBlocks();
            }
            //start again
            blocks = new List<IBlock>();
        }

        public BlockReleaser scoreCombiner;
        public int minCombine = 3;

        protected void CombineBlocks()
        {
            //Much of the work of actually spawning blocks has been moved to other scripts
            if (blocks.Count < minCombine)
            {
                ReleaseBlocks();
            }
            else
            {
                ConvertAllRainbows();
                //if (blocks.Count >= bonusBlocks)
                //{
                    //bonus time!
                  //  if(bonus!=null)
                        bonus.ExecuteBonus(blocks);
                //}
                scoreCombiner.ReleaseBlocks(blocks);
            }
        }

        //Make all rainbows appear to scripts to be the same as other blocks
        //Without animation, this is only safe right before blocks are deleted
        void ConvertAllRainbows()
        {
            foreach(IBlock block in blocks)
            {
                if(block.blockColor.value == BlockColor.rainbowColor)
                {
                    block.blockColor.value = curColor.value;
                }
            }
        }

        public Bonus bonus;

        public AudioSource failSound;

        public void ReleaseBlocks()
        {
            lines.Clear();
            graphics.Clear();
            failSound.Play();
            foreach (IBlock block in blocks)
            {
                block.Release();
            }
        }
    }
}
