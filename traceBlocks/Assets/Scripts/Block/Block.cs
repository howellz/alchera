using UnityEngine;
using System.Collections;

namespace BlockGame
{
    /// <summary>
    /// Goes on most blocks in the game
    /// Allows them to be grabbed by the BlockSelector
    /// </summary>
    public class Block : IBlock
    {
        protected BlockSelection blockSelector;
        public bool held = false;  //did I get picked up by block selector?
        public Color highlightColor = new Color(1f, 1f, 1f, 0.5f);
        public Color normalColor = Color.white;

        protected override void Start()
        {
            base.Start();
            blockSelector = BlockSelection.Instance;
            //set default value - may need better solution if I ever want to change this in inspector
            renderer.material.color = normalColor;
        }

        public override void OnHover()
        {
            if (Input.GetMouseButton(0))// && !held) ///blocks can also be deselected
            {
                if (blockSelector.SelectBlock(gameObject))
                {
                    //GetGrabbed();
                }
            }
        }

        public override bool PoliteKill()
        {
            if (!held)
            {
                GameObject.Destroy(gameObject);
                return true;
            }
            return false;
        }

        public override void GetGrabbed()
        {
                held = true;
                renderer.material.color = highlightColor;
        }

        public override void Release()
        {
            if (held)
            {
                held = false;
                renderer.material.color = normalColor;
            }
        }
    }

}