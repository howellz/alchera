﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Show placeholder images for what blocks are going to be created when player combines select blocks
    /// </summary>
    public class BlockSelectGraphics : MonoBehaviour
    {
        protected List<IBlock> blocks;

        //A mapping from block colors to a placeholder image for those colors
        public List<Transform> blockImagePrefabs = new List<Transform>();
        //The placeholder value of gold
        public Transform goldPrefab;
        protected ExtraBlocksBonus blocksDecider;
        protected IList<Transform> blocksToSpawn = null;
        //protected IList<Transform> followingBlocks = null;
        protected Transform blockParent;
        //Play a sound when they get a new block
        public List<AudioClip> blockSelectionSounds = new List<AudioClip>();
        public AudioSource audioSource;

        protected void Start()
        {
            blocksDecider = BlockSelection.Instance.GetComponent<ExtraBlocksBonus>();
            blockParent = GameObject.Find("BlocksToSpawnParent").transform;
            if(audioSource==null)
            {
                audioSource = gameObject.GetComponent<AudioSource>();
            }
        }

        internal void Initialize(IBlock block)
        {
            blocks = new List<IBlock>();
            blocks.Add(block);
        }

        internal void RemoveBlock(int blockIndex)
        {
            blocks.RemoveAt(blockIndex);
            UpdateBlocksToSpawn();
        }

        internal void Clear()
        {
            blocks = null;
            DestroyBlocksToSpawn();
        }

        internal void AddBlock(IBlock block)
        {
            blocks.Add(block);
            UpdateBlocksToSpawn();
            //Play a sound
            if(audioSource!=null)
            {
                //Play a new sound for every extra block
                int blockNum = blocks.Count-1;
                if(blockNum>=blockSelectionSounds.Count)
                {
                    blockNum = blockSelectionSounds.Count - 1;
                }
                audioSource.PlayOneShot(blockSelectionSounds[blockNum]);
            }
        }

        protected void UpdateBlocksToSpawn()
        {
            //Should rework this to not destroy all the prefabs every time block added
            DestroyBlocksToSpawn();
            //Add new placeholders
            IEnumerable<IBlock> upgradedBlocks = blocksDecider.GetBlocksToUpgrade(blocks);
            blocksToSpawn = new List<Transform>();
            foreach (IBlock b in upgradedBlocks)
            {
                Transform newImage;
                if (b.blockColor.CanAdvanceToNextLevel())
                {
                    newImage = (Transform)GameObject.Instantiate(blockImagePrefabs[b.blockColor.value + 1], b.transform.position, Quaternion.identity);
                }
                else
                {
                    newImage = (Transform)GameObject.Instantiate(goldPrefab, b.transform.position, Quaternion.identity);
                }
                newImage.parent = blockParent;
                blocksToSpawn.Add(newImage);
            }
        }

        protected void DestroyBlocksToSpawn()
        {
            //Get blocks to spawn placeholders
            if (blocksToSpawn != null)
            {
                //Destroy old blocks
                foreach (Transform t in blocksToSpawn)
                {
                    GameObject.Destroy(t.gameObject);
                }
                blocksToSpawn = null;
            }
        }


    }
}
