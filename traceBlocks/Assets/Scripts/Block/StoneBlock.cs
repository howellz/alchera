﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    //Stone block can never be held so don't even try
    //Put this on the stone block
    public class StoneBlock : IBlock
    {
        public override void OnHover()
        {
        
        }

        public override void GetGrabbed()
        {
        }

        public override void Release()
        {

        }

    }
}
