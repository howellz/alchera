using UnityEngine;
using System.Collections;

namespace BlockGame
{
    /// <summary>
    /// An abstract class for blocks.
    /// Provides most of the functionality that all blocks require
    /// Basically everything but GetGrabbed, OnHover, and Release
    /// </summary>
    public abstract class IBlock : MonoBehaviour
    {
        protected GridManager mainGrid;
        
        public BlockColor blockColor;  

        protected virtual void Start()
        {
            mainGrid = GridManager.instance;
            mainGrid.SnapToGrid(transform);
            
            //Set all block prefabs to "Untagged"; they'll be set programatically later
//            tag = "Untagged";
        }

        public void CopyValues(IBlock block)
        {
            this.blockColor = block.blockColor;
        }

        public abstract void OnHover();

        public abstract void GetGrabbed();

        public abstract void Release();

        public virtual bool PoliteKill()
        {
            GameObject.Destroy(gameObject);
            return true;
        }

        protected bool appQuit = false;

        protected void OnApplicationQuit()
        {
            appQuit = true;
        }

        protected void OnDestroy()
        {
            if(!appQuit && !LevelChanger.levelIsEnding)
            {
                //Increment score
                int score = blockColor.value;
                if(blockColor.value == BlockFactory.rainbowNum)
                {
                    score = 5;
                }
                else if(blockColor.value == BlockFactory.stoneNum)
                {
                    score = 5;
                }
                score *= 10;
                ScoreManager.Instance.GetScore().AddToScore(score);

                //Also add to gold counter
                if(ScoreManager.Instance.gold!=null && blockColor.IsGold())
                {
                    ScoreManager.Instance.gold.AddToScore(1);
                }

                if(upgradeEffect!=null)
                {
                    GameObject.Instantiate(upgradeEffect,transform.position,Quaternion.identity);
                }
            }
        }

        public GameObject upgradeEffect;

        public GameObject upgradedCreateEffect;

        public void ShowCreatedEffect()
        {
            if(upgradedCreateEffect!=null)
            {
                GameObject.Instantiate(upgradedCreateEffect, transform.position, Quaternion.identity);
            }
        }
    }

}