﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    //Spawn all the blocks for combining!
    //Not really bonus blocks any more, just spawn all of them
    public class ExtraBlocksBonus : Bonus
    {
        protected BlockUpgrader upgrader = null;
        
        public int moreBlocks = 2;
        protected int offsetNum = 1;

        protected void Start()
        {
            if (upgrader == null)
                upgrader = (BlockUpgrader)Toolbox.Instance.GetOrAddComponent<BlockUpgrader>();
        }

        /// <summary>
        /// Spawn all the blocks they deserve!
        /// </summary>
        /// <param name="blocks"></param>
        public override void ExecuteBonus(IEnumerable<IBlock> blocks)
        {
            //Which blocks are they spawning?
            blocks = GetBlocksToUpgrade(blocks);
            //Spawn them!
            SpawnExtraBlocks(blocks);
        }

        public void SpawnExtraBlocks(IEnumerable<IBlock> blocks)
        {
            foreach(IBlock block in blocks)
            {
                upgrader.CreateUpgradeBlock(block);
            }
        }

        /// <summary>
        /// Select/return an appropriate number of blocks from end of chain
        /// </summary>
        /// <param name="blocks"></param>
        /// <returns></returns>
        public IEnumerable<IBlock> GetBlocksToUpgrade(IEnumerable<IBlock> blocks)
        {
            //How many blocks are they getting?
            int numBlocks = blocks.Count();
            //numBlocks -= offsetNum;
            //numBlocks /= moreBlocks;
            numBlocks = GetHowManyBlocks(numBlocks);

            return GetBlocksToUpgrade(blocks, numBlocks);
        }

        public int GetHowManyBlocks(int numCombined)
        {
            int numBlocks;
            if (numCombined <= 4)
            {
                numBlocks = 1;
            }
            else if (numCombined <= 6)
            {
                numBlocks = 2;
            }
            else
            {
                //give them one block for every block 7+
                numBlocks = 3 + (numCombined - 7);
            }
            return numBlocks;
        }

        /// <summary>
        /// Select the numBlocks blocks at end of chain
        /// </summary>
        /// <param name="blocks"></param>
        /// <param name="numBlocks"></param>
        /// <returns></returns>
        protected IEnumerable<IBlock> GetBlocksToUpgrade(IEnumerable<IBlock> blocks, int numBlocks)
        {
            IList<IBlock> newBlocks = new List<IBlock>();
            //Spawn blocks from end of list
            blocks = blocks.Reverse();
            for (int i = 0; i < numBlocks; i++)
            {
                IBlock lastBlock = blocks.Skip(i).FirstOrDefault();
                if (lastBlock != null)
                {
                    newBlocks.Add(lastBlock);
                }
            }
            return newBlocks;
        }
    }
}
