﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public abstract class Bonus : MonoBehaviour
    {

        public abstract void ExecuteBonus(IEnumerable<IBlock> blocks);
    }
}
