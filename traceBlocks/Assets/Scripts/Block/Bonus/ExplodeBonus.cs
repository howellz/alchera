﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class ExplodeBonus : Bonus
    {
        public Explosion explosion;

        public override void ExecuteBonus(IEnumerable<IBlock> blocks)
        {
            if (explosion != null)
            {
                GridVector explodePos = GridManager.instance.xyzToij(blocks.LastOrDefault().transform.position);

                explosion.Explode(DeterminePositions(explodePos));
            }
        }

        public int numRows = 3;
        public int numColumns = 3;

        public IEnumerable<GridVector> DeterminePositions(GridVector explodeij)
        {
            List<GridVector> positions = new List<GridVector>();
            int iOffset = (int)Mathf.Floor(numColumns / 2);
            int jOffset = (int)Mathf.Floor(numRows / 2);
            for (int i = 0; i < numColumns; i++)
            {
                for (int j = 0; j < numRows; j++)
                {
                    if (i == iOffset && j == jOffset)
                    {
                        //we shouldn't explode the center
                        continue;
                    }
                    positions.Add(new GridVector(explodeij.x + i - iOffset, explodeij.y + j - jOffset));
                }
            }
            return positions;
        }
    }
}
