﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class LineExplodeBonus : Bonus
    {
        public IExplosion explosion;
        private GridManager grid;

        void Start()
        {
            grid = GridManager.instance;
        }

        public override void ExecuteBonus(IEnumerable<IBlock> blocks)
        {
            if (explosion != null)
            {
                foreach(IBlock block in blocks)
                {
                    GridVector explodePos = grid.xyzToij(block.transform.position);
                    explosion.Explode(DeterminePositions(explodePos));
                }
            }
        }

        //the functionality below here could be abstracted into another script

        public IEnumerable<GridVector> DeterminePositions(GridVector explodeij)
        {
            List<GridVector> positions = new List<GridVector>();
            for (int i = 0; i < grid.numColumns; i++)
            {
                positions.Add(new GridVector(i, explodeij.y));
            }
            return positions;
        }
    }
}
