﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class BlockEventCombiner : BlockReleaser
    {
        public BlockReleaser combiner;
        public TurnBasedEvents events;

        public override void ReleaseBlocks(IEnumerable<IBlock> blocks)
        {
            combiner.ReleaseBlocks(blocks);
            events.BlocksCombined();
        }
    }
}
