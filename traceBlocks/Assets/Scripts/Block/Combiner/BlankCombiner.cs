﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class BlankCombiner : BlockReleaser
    {
        public override void ReleaseBlocks(IEnumerable<IBlock> blocks)
        {
            sound.Play();
            foreach(IBlock block in blocks)
            {
                IBlock blockScript = block.GetComponent<IBlock>();
                if(blockScript!=null)
                {
                    blockScript.Release();
                }
            }
        }
    }
}
