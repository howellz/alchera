﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    //
    public class GoldCombiner : BlockReleaser
    {
        public ScoreCombiner scoreCombiner;

        private BlockUpgrader upgrader = null;

        void Start()
        {
            if(upgrader==null)
                upgrader = (BlockUpgrader)Toolbox.Instance.GetOrAddComponent<BlockUpgrader>();
        }

        public override void ReleaseBlocks(IEnumerable<IBlock> blocks)
        {
            sound.Play();
            IBlock destBlock = blocks.LastOrDefault();
            
            //Is gold exploding?
            if (!destBlock.blockColor.CanAdvanceToNextLevel())//upgrader.CreateUpgradeBlock(destBlockScript))//
            {
                scoreCombiner.ReleaseBlocks(blocks);
            }
    
            //Destroy all the blocks
            foreach (IBlock block in blocks)
            {
                if (block == null)
                {
                    continue;
                }
                GameObject.Destroy(block.gameObject);
            }

        }
    }
}
