﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public abstract class BlockReleaser : MonoBehaviour
    {
        public AudioSource sound;
        public abstract void ReleaseBlocks(IEnumerable<IBlock> blocks);
    }
}
