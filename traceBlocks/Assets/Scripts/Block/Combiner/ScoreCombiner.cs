﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    //Explode a line of blocks and spawn score objects
    public class ScoreCombiner : BlockReleaser
    {
        public GameObject scorePrefab;
        public LineExplodeBonus noMoreBlocksBonus;

        public override void ReleaseBlocks(IEnumerable<IBlock> blocks)
        {
            if(noMoreBlocksBonus!=null)
                noMoreBlocksBonus.ExecuteBonus(blocks);
            sound.Play();
            foreach (IBlock block in blocks)
            {
                Vector3 blockPos = block.transform.position;
                if(scorePrefab!=null)
                {
                    GameObject score = (GameObject)GameObject.Instantiate(scorePrefab, blockPos, Quaternion.identity);
                }
                //Not deleting the blocks (leave it to be deleted by GoldCombiner)
            } 
        }
    }
}
