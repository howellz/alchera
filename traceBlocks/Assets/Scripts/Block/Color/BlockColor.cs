﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    //basically a fancy enum
    [System.Serializable]
    public class BlockColor
    {
        public int value;

        public BlockColor()
        {
        }

        public BlockColor(int color)
        {
            this.value = color;
        }

        //Exported information about number of colors, etc to BlockColorManager
        private BlockFactory manager
        {
            get
            {
                if(_manager==null)
                {
                    _manager = (BlockFactory)Toolbox.Instance.GetOrAddComponent<BlockFactory>();
                }
                return _manager;
            }
        }
        private BlockFactory _manager;

        //change color to a "higher" level
        public bool CanAdvanceToNextLevel()
        {
            return manager.CanAdvance(this);
        }

        //operator overloads
        public static bool operator ==(BlockColor c1, BlockColor c2)
        {
            return (c1.value==c2.value);
        }
        public static bool operator !=(BlockColor c1, BlockColor c2)
        {
            return (c1.value!=c2.value);
        }

        public bool IsGold()
        {
            return value==4;
        }

        public static BlockColor Red = new BlockColor(0);
        public static BlockColor Purple = new BlockColor(1);
        public static BlockColor Blue = new BlockColor(2);
        public static BlockColor Green = new BlockColor(3);
        public static BlockColor Gold = new BlockColor(4);

        //Allow for rainbow blocks to be anything
        public static int rainbowColor = 77;
        public static int stoneColor = 111;
        public bool IsEqual(BlockColor c2)
        {
            return (value == c2.value || value == rainbowColor || c2.value == rainbowColor);
        }

    }
}
