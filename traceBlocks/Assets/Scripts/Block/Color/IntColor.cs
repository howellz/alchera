﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public static class IntColor
    {
        public static Color Create(int r, int g, int b, int a=256)
        {
            return new Color(r / 256f, g / 256f, b / 256f, a / 256f);
        }
    }
}
