﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Just a singleton to put on the parent of all the blocks gameobject
    ///     Let's add counting functionality
    /// </summary>
    public class BlockParent : Singleton<BlockParent>
    {
        public int totalBlocks
        {
            get
            {
                return transform.childCount;
            }
        }

        public int GetNumBlocksValue(BlockColor color)
        {
            return GetNumBlocksValue(color.value);
        }

        //How many blocks have the specified BlockColor value?
        public int GetNumBlocksValue(int color)
        {
            //search all the children
            int totalOfColor = 0;
            foreach(Transform child in transform)
            {
                IBlock block = child.GetComponent<IBlock>();
                if(block!=null)
                {
                    if (block.blockColor.value == color)
                    {
                        totalOfColor += 1;
                    }
                }
                else
                {
                    Debug.Log("block color is null?");
                }
            }
            return totalOfColor;
        }
    }
}
