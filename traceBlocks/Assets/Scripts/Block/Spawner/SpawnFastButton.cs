﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class SpawnFastButton : MonoBehaviour
    {
        private WaveSpawner spawner;
        public GameObject fastIndicator;
        protected bool heldDown = false;
        protected bool hover = false;

        void Start()
        {
            spawner = SpawnerObject.Instance.GetOrAddComponent<WaveSpawner>();
        }

        protected void OnMouseOver()
        {
            if(Input.GetMouseButtonDown(0))// && !heldDown)
            {
                spawner.SpawnWave();
                fastIndicator.SetActive(true);
                Invoke("IndicatorOff", indicatorTime);
              //  heldDown = true;
            }
        }

        private float indicatorTime = 0.7f;

        protected void Update()
        {
         //   fastIndicator.SetActive(hover);
           // heldDown = false;
           // hover = false;
        }

        public void IndicatorOff()
        {
            fastIndicator.SetActive(false);
        }
    }
}
