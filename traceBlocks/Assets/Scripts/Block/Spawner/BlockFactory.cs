﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    /// <summary>
    /// Manages the actual instantiation of blocks in the scene
    /// Basically - spawn a block of a certain color
    /// Also manages all the different block colors - ie, does a color exist?  get me a random color
    /// </summary>
    public class BlockFactory : MonoBehaviour
    {
        protected int numColors;
        public bool setParent = true;
        private Transform blockParent;

        protected void Start()
        {
            numColors = colorMap.Count;
            blockParent = GameObject.Find("AllBlocks").transform;
        }


        #region Create a Block
        /// <summary>
        /// Create a block gameobject of value color at the position
        /// The value is mapped to a prefab of correct color
        /// </summary>
        /// <param name="color">Value/color of desired block</param>
        /// <param name="position">Position to create the block at</param>
        /// <returns>The created block.  If no prefab block with that color exists, returns null.</returns>
        public GameObject CreateBlock(int color, Vector3 position)
        {
            GameObject prefab;
            if(color<numColors)
            {
                prefab = colorMap[color];
            }
            else if(color==rainbowNum)
            {
                prefab = rainbowBlock;
            }
            else if(color==stoneNum)
            {
                prefab = stoneBlock;
            }
            else
            {
                return null;
            }
            GameObject block = GameObject.Instantiate(prefab, position, Quaternion.identity) as GameObject;
            block.GetComponent<Block>().blockColor.value = color;

            //Automatically set the parent
            if(setParent && blockParent!=null)
            {
                block.transform.parent = blockParent;
            }
            return block;
        }
        #endregion

        #region Manage Block Color List
        //Below here manage the block color list and increasing color values
        //--------------------------------------------------------------------
        public List<GameObject> colorMap = new List<GameObject>();

        //are there any more colors?
        public bool CanAdvance(BlockColor color)
        {
            int nextColor = color.value + 1;
            return nextColor < numColors;
        }

        public int GetNextColor(BlockColor color)
        {
            if (color.value < numColors)
            {
                return color.value + 1;
            }
            //um, return a bogus value?
            return 1000;
        }

        public bool IsGold(BlockColor color)
        {
            return (color.value == numColors - 1);
        }
        #endregion

        #region Stone/Rainbow blocks
        //Allow spawning of rainbow and stone blocks that don't use the colorMap hierarchy
        //--------------------------------------------------------------------------------
        public static int rainbowNum = BlockColor.rainbowColor;
        public static int stoneNum = BlockColor.stoneColor;
        public GameObject rainbowBlock;
        public GameObject stoneBlock;
        #endregion

        #region Choose Random Blocks
        //IBlock number to start from so we can cut off bottom blocks instead of top
        public int minBlockSpawn = 0;
        public int maxBlockSpawn = 2;

        //The chance of spawning each block, corresponding to the gameobjects in the colorMap list
        //The total values should add up to 1f
        public List<float> blockChances = new List<float>();

        //These values are checked before choosing a normal block
        public float rainbowChance = 0.08f;
        public float stoneChance = 0.05f;

        public virtual int GetRandomBlockColor()
        {
            //Check to spawn a rainbow or stone block first
            if(UnityEngine.Random.Range(0f,1f)<=rainbowChance)
            {
                return rainbowNum;
            }
            if (UnityEngine.Random.Range(0f, 1f) <= stoneChance)
            {
                return stoneNum;
            }

            //Choose which normal block to spawn
            float r = UnityEngine.Random.Range(0f, 1f);
            //Figure out which block corresponds to the chose number
            //By checking each chance in a row if it's under that chance but higher than previous chances
            float chanceSoFar = 0.0f;
            int curBlock = 0;
            foreach(float chance in blockChances)
            {
                if(r<=chance+chanceSoFar)
                {
                    return curBlock;
                }
                curBlock += 1;
                chanceSoFar += chance;
            }

            //We should never get here, but I guess randomly return one of the possible blocks
            int rN = UnityEngine.Random.Range(minBlockSpawn, maxBlockSpawn+1);
            return rN;
        }

        public GameObject SpawnRandomBlock(Vector3 position)
        {
            int r = GetRandomBlockColor();
            return CreateBlock(r,position);
        }
        #endregion
    }
}
