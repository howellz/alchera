using UnityEngine;
using System.Collections;

namespace BlockGame
{
    public class BlockSpawner : MonoBehaviour
    {
        private GridManager mainGrid;

        public int numBlocksSpawned = 0;
        private BlockFactory factory;
        
        public void Awake()
        {
            mainGrid = GridManager.instance;
        }

        void Start()
        {
            factory = (BlockFactory)Toolbox.Instance.GetOrAddComponent<BlockFactory>();
        }

        //Determine position by using one of the below functions
        //These should be in another object, but I got lazy with all these interconnecting parts...
        #region Determine Position
        private int curPosition = 0;

        ColumnTracker colTracker = null;

        //Return a position that just increments, ie, spawn one block at each position 
        public int GetIncrementingPosition()
        {
            AdvancePosition();
            return curPosition;
        }

        public int AdvancePosition()
        {
            curPosition += 1;
            curPosition = curPosition % mainGrid.numColumns;
            return curPosition;
        }

        public int savePercentage = 80;

        //Return a position that is less likely to kill the player
        public int GetSmartPosition()
        {
            if (colTracker == null)
                colTracker = Toolbox.Instance.GetOrAddComponent<ColumnTracker>();
            AdvancePosition();
            while(colTracker.IsColumnWarning(curPosition) && Random.Range(0,100)<savePercentage)
            {
                AdvancePosition();
            }
            return curPosition;
        }

        //Return a position that won't kill the player
        public int GetEasyPosition()
        {
            if (colTracker == null)
                colTracker = Toolbox.Instance.GetOrAddComponent<ColumnTracker>();

            int numTimesAdvanced = 0;
            AdvancePosition();
            while (colTracker.IsColumnWarning(curPosition) && numTimesAdvanced<mainGrid.numColumns)
            {
                AdvancePosition();
                numTimesAdvanced++;
            }
            return curPosition;
        }

        //Return a position that won't kill the player
        //return a position that favors middle over edges
        public int GetMiddleLikelyPosition()
        {
            if (colTracker == null)
                colTracker = Toolbox.Instance.GetOrAddComponent<ColumnTracker>();

            int numTimesAdvanced = 0;
            AdvancePosition();

            float skipEdgeChance = 0.2f;
            if(curPosition==0 || curPosition == mainGrid.numColumns-1)
            {
                if(Random.Range(0f,1f)<=skipEdgeChance)
                {
                    //skip that edge
                    AdvancePosition();
                }
            }

            while (colTracker.IsColumnWarning(curPosition) && numTimesAdvanced < mainGrid.numColumns)
            {
                AdvancePosition();
                numTimesAdvanced++;
            }
            return curPosition;
        }

        protected int lastSpawnPoint = -1;

        //Return a position that favors empty columns over tall ones\
        //And returns a position that won't kill player
/*        public int GetLikelyEmptyPosition()
        {
            if (colTracker == null)
                colTracker = Toolbox.Instance.GetOrAddComponent<ColumnTracker>();

            //tally up the total heights, inversely proportional to the actual height
            //weighing the shorter columns heavier than the taller ones
            int sumHeights = 0;
            foreach(int colHeight in colTracker.columnHeights)
            {
                sumHeights += (mainGrid.numRows-colHeight);
            }
            //select one of those columns at random
            int r = Random.Range(0, sumHeights);
            //actually figure out which column we selected by going through again
            int newSum = 0;
            int i = 0;
            while(newSum<=r)
            {
                newSum += (mainGrid.numRows - colTracker.columnHeights[i]);
                if(newSum<=r)
                {
                    //otherwise we found it
                    i += 1;
                }
            }
            //we found our column, set it to it
            curPosition = i;

            //failsafe, check to make sure that column isn't too tall, or that we can drop a block anywhere
            int numTimesAdvanced = 0;
            while ( (lastSpawnPoint == curPosition || colTracker.IsColumnWarning(curPosition)) && numTimesAdvanced < mainGrid.numColumns)
            {
                AdvancePosition();
                numTimesAdvanced++;
            }
            lastSpawnPoint = curPosition;
            return curPosition;
        }

        public int GetSemiRandomPosition()
        {
            if (colTracker == null)
                colTracker = Toolbox.Instance.GetOrAddComponent<ColumnTracker>();
            
            //choose a random number more likely to be on a block with fewer values
            int sum = 0;
            int maxHeight = mainGrid.numRows;
            foreach(var height in colTracker.columnHeights)
            {
                sum = sum + (maxHeight - height);
            }
            int r = Random.Range(0, sum);

            //then go back up and find which column we chose
            sum = 0;
            int i = 0;
            foreach(var height in colTracker.columnHeights)
            {
                sum += (maxHeight + 1 - height);
                if (sum > r)
                    return i;
                i++;
            }
            //we should have already found a value
            return i;
        }*/
        #endregion

        public bool spawnSmart = true;
        public void SpawnBlock()
        {
            int pos;
            if (spawnSmart)
            {
                pos = GetMiddleLikelyPosition();
            }
            else pos = GetIncrementingPosition();
            SpawnBlock(pos);
        }

        //Spawn a block in the column col
        public void SpawnBlock(int col)
        {
            //become disabled when the gameobject does
            if(!gameObject.activeSelf)
            {
                return;
            }
            //spawn at top of grid, at specified column col
            Vector3 spawnPos = mainGrid.ijToxyz(new GridVector(col,mainGrid.numRows));
            Transform newBlock = factory.SpawnRandomBlock(spawnPos).transform;

            //give it a nice name and organize it
            newBlock.gameObject.name += numBlocksSpawned;
            numBlocksSpawned += 1;
        }
    }
}