using UnityEngine;
using System.Collections;
using Vectrosity;
using System.Collections.Generic;
using System.Linq;

namespace BlockGame
{
    public class UserInput : MonoBehaviour
    {
        private GridManager grid;

        protected Pauser pauser;
        //Don't send messages to stuff with this tag while paused
        public string pauseTag = "Block";

        private void Start()
        {
            grid = GridManager.instance;
            pauser = Pauser.Instance;
        }

        private void Update()
        {
            //Get location of mouse and send out messages to everything it touched
            ClickAtWorldPoint(InputMouse.worldPosition);//Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

        //Send out messages to all blocks at screenPos
        private void ClickAtWorldPoint(Vector3 pos)
        {
            //round to nearest grid center
        //    GridVector ijPos = grid.xyzToij(pos);
          //  pos = grid.ijToxyz(ijPos);
            IEnumerable<RaycastHit2D> hits = grid.GetHitsAtPos(pos);
            foreach (RaycastHit2D hit in hits)
            {
                //Don't send out messages while paused to some objects
                if(!pauser.isPaused || hit.transform.tag != pauseTag)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        hit.transform.gameObject.SendMessage("OnClick", null, SendMessageOptions.DontRequireReceiver);
                    }
                    else
                    {
                        hit.transform.gameObject.SendMessage("OnHover", null, SendMessageOptions.DontRequireReceiver);
                    }
                }
                //Debug.Log("hover on :" + hit.transform.gameObject.name);
            }
            //we didn't hit any blocks
            if(Input.GetMouseButton(0) && emptySpace!=null && !hits.Where(h=>h.collider.tag=="Block").Any())
            {
                emptySpace.Click(pos);
            }
        }

        public NothingThere emptySpace;


        //"Click" on all blocks on the line formed by start and end
        public void ClickOnLine(Vector2 start, Vector2 end)
        {
            Vector3 start3 = Camera.main.ScreenToWorldPoint(start);
            Vector3 end3 = Camera.main.ScreenToWorldPoint(end);
            Vector3 dir = (end3 - start3);
            float length = dir.magnitude;
            dir = dir.normalized;
            //attempt to check only as many times as there could be blocks in position
            int numBlocks = (int) (length / grid.xGridSize);
            for(int i=0;i<=numBlocks;i++)
            {
                ClickAtWorldPoint(start3 + dir * i * grid.xGridSize);
            }
        }
    }

}