﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockGame
{
    public class LossChecker : MonoBehaviour
    {
        //switched from column based killing to block number based killing
        private BlockParent blockParent;
        public float warningPercentage = .8f;
        protected int numBlocksWarning = 55;
        protected int numBlocksDead = 70;

        //after blocks reach top, give them this extra time (also helps prevent a bug with a few extra blocks appearing)
        public float extraTime = 1f;

        public GameObject warning;
		public ColorChangeOverTime warningBlink;

        //Migrated a lot of work to columntracker, can start using it
        private ColumnTracker cols;

        protected void Start()
        {
            blockParent = BlockParent.Instance;
            //set number of blocks to kill player from grid
            GridManager grid = GridManager.instance;
            numBlocksDead = grid.numColumns * grid.numRows;
            numBlocksWarning = (int) (numBlocksDead * warningPercentage);
        }

        protected void Update()
        {
            //loop through the top row of grid positions
            if(blockParent.totalBlocks>numBlocksWarning)
            {
                ShowWarning();
            }
            else
            {
                HideWarning();
            }
            if (blockParent.totalBlocks>numBlocksDead)
            {
                Invoke("CheckGameOver",extraTime);
            }
        }

        protected void CheckGameOver()
        {
            if(blockParent.totalBlocks>numBlocksDead)
            {
                GameOver();
            }
        }


        public void ShowWarning()
        {
            if(warning!=null)
            {
                warning.SetActive(true);
            }
			warningBlink.TurnOn ();
        }

        public void HideWarning()
        {
			if (warning != null) {
				warning.SetActive (false);
			}
			warningBlink.TurnOff ();
        }

        private void GameOver()
        {
            //HideWarning();
            this.enabled = false;
            WinLoseEffects.Instance.GameOver();
        }
    }
}
