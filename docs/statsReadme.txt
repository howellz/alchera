The stats in there affect the difficulty of the game.  In order,
minSpawnTime - how much time between block "wave"s spawning at max difficulty.  The game gets more difficult (speeding up by 1) as time goes by.
maxSpawnTime - how much time between block "wave"s spawning at start of game.
hardFactor - how much more difficult the game is during hard waves.  Basically blocks spawn at spawnTime*hardFactor speed during hard waves.
numBlocksInWave - how many blocks spawn after spawnTime (a number between min and maxSpawnTime) has passed.
minBlockColor - which color of block to spawn on the low end (usually 0 for red line or 1 for purple triangle).  Setting this lower makes the game harder.
maxBlockColor - the higher color of block to spawn (usually 2 for blue diamond or 3 for green star).  Setting a large min/max range (ex 0-3) means the user can make fewer possible moves.